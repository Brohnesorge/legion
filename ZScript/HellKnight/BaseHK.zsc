class LEG_BaseHK : LEG_BaseMonster
{
	bool  	BeastMode;
	int   	RageMeter;
	int 	flamex;
	int		flamey;
	
	action void A_LEGKnightCharge(string VFXActor1 = "LEG_GreenImpTrail", string VFXActor2 = "LEG_BaronFlame")
	{
		A_FaceTarget();
		invoker.AttackCharge += randompick(0, 1, 1);
		
		A_SpawnItemEx(VFXActor1, frandom(-6, 6), bSpriteFlip ? -invoker.flamex : invoker.flamex, invoker.flamey);
		
		if(invoker.AttackCharge > invoker.AttackThreshold * 0.5)
		{
			A_SpawnItemEx(VFXActor1, 6, bSpriteFlip ? -invoker.flamex : invoker.flamex, invoker.flamey);
			A_SpawnItemEx(VFXActor1, 6, bSpriteFlip ? -(invoker.flamex * frandom(0.7, 1.3)) : (invoker.flamex * frandom(0.7, 1.3)), invoker.flamey * frandom(1.0, 1.2));
			A_SpawnItemEx(VFXActor2, 6, bSpriteFlip ? -(invoker.flamex * frandom(0.7, 1.3)) : (invoker.flamex * frandom(0.7, 1.3)), invoker.flamey * frandom(1.0, 1.2));
		}
	}

	action void A_LEGKnightBall()
	{
		A_SpawnProjectile("LEG_KnightBall", 32, 0, frandom(-3, 3));
		A_StartSound("Baron/Cast", 200, CHANF_DEFAULT, 1.0, 1.33);
		invoker.RageMeter += 1;
		invoker.AttackCharge = 0;
	}

	action void A_LEGKnightMelee()
	{
		A_CustomBulletAttack(0, 0, 1, 10 * random(1, 8), "LEG_BaronPuff", 64, CBAF_AIMFACING | CBAF_NORANDOM);
		invoker.AttackCharge = 0;
		if (invoker.BeastMode == TRUE)
		{
			A_SetTics(4);
		}

		invoker.RageMeter += 2;
	}
  
	action void A_KnightDeath()
	{
		A_ScreamAndUnblock();
		A_TakeInventory("LEG_RageGiver");
		A_TakeInventory("LEG_KnightRage");
		invoker.BeastMode = FALSE;
	}

	action void A_KnightRageMode()
	{
		invoker.BeastMode = TRUE;
		A_GiveInventory("LEG_RageGiver", 1);
		A_StartSound(SeeSound, 198, CHANF_DEFAULT, 1.0, ATTN_NONE, 0.8);
		A_RadiusThrust(512, 128, RTF_NOTMISSILE);
	}
	
	action void A_KnightPain()
	{
		if(LEG_Extra) invoker.RageMeter += 10; 
		A_LEGPain();
	}

	override void tick()
	{
		if(self.BeastMode == TRUE && !random(0, 3))
		{
			bBuddha = TRUE;
		}
		else bBuddha = FALSE;

		super.Tick();
	}
	
	override void BeginPlay()
	{
		BaseMonster 	= "HellKnight";
		VanillaMonster 	= "LEG_HellKnight";
		
		BeastMode      	 = FALSE;
		RageMeter      	 = 0;
		flamex			 = 28;
		flamey			 = 55;
		
		AttackThreshold 	= 14;
		Attack2Threshold	= 10;
		SpecialReadyTime	= 175;
	
		super.BeginPlay();
	}

	Default
	{
		Health      500;
		GibHealth   200;
		Speed       8;
		Height      64;
		Radius      16;
		Mass        500;
		PainChance  75;
		MeleeRange  96;
		MeleeThreshold 196;
		Scale       0.95;

		DamageFactor "Unholy",    0.50;
		DamageFactor "Holy",      2.00;
		DamageFactor "Magic",     0.75;
		DamageFactor "Fire",      0.75;
		DamageFactor "Ice",       0.75;
		DamageFactor "Electric",  0.75;

		Tag         "Hell Knight";
		Obituary    "Torn apart by a Hell Knight";
		
		Species     "Knight";
		SeeSound    "knight/sight";
		PainSound   "knight/pain";
		DeathSound  "knight/death";
		ActiveSound "knight/active";
		
		BloodColor  "Green";

		+FLOORCLIP;
		+QUICKTORETALIATE;
		+JUMPDOWN;
		+DOHARMSPECIES;
		+FORCEINFIGHTING;
		+MISSILEMORE;
	}
}

class LEG_KnightBall : LEG_BaseProj
{
	Default
	{
		Speed   15;
		Gravity 0.01;
		Alpha   0.85;
		Scale   0.35;

		DamageType  "Magic";

		-NOGRAVITY;
		
		LEG_BaseProj.BaseDamage 7;
		LEG_BaseProj.VFXtype "LEG_BaronFlame";
	}

	States
	{
		Spawn:
		LFB3 ABCDEDCB 1 NoDelay
		{
			A_StartSound("Legion/FireBurn", 201, CHANF_LOOPING, 0.45, 1.0, 0.75);
			A_TailSpawn(4);
		}
		Loop;

		Death:
		XDeath:
		Crash:
			LFB3 F 1
			{
				if(LEG_Flames)
					{
					A_SpawnItemEx("LEG_BBFlame", 0, 0, 0, -1, frandom(-3, 3));
					A_SpawnItemEx("LEG_BBFlame", 0, 0, 0, -1, frandom(-3, 3));
					A_SpawnItemEx("LEG_BBFlame", 0, 0, 0, -1, frandom(-3, 3));
				}
				if(LEG_Smoke)
				{
					A_SpawnItemEx("LEG_FireballSmoke");
					A_SpawnItemEx("LEG_FireballSmoke", frandom(-5, 5), frandom(-5, 5), frandom(-5,5), 0, frandom(-3, 3), frandom(0,2));
				}
				A_StartSound("Baron/BallHit", 201);
				A_SetScale(1.0);
				A_SetRenderstyle(0.15, STYLE_ADD);
			}
			LFB3 FFF 1 A_SetScale(Scale.X + 0.25);
			Stop;
	}
}

class LEG_KnightRage : PowerProtection
{
	Default
	{
		DamageFactor "Normal", 0.5;

		+NOPAIN;
		+DONTTHRUST;
		+DONTRIP;
	}
}

class LEG_RageGiver : PowerupGiver
{
	Default
	{
		Powerup.Duration 0x7FFFFFFD;
		Powerup.Type "LEG_KnightRage";

		+INVENTORY.AUTOACTIVATE;
	}
}

class LEG_HKSpawner : RandomSpawner replaces HellKnight
{
	Default 
	{
		DropItem "LEG_HellKnight", 255, 75;
		DropItem "LEG_CyberKnight", 255, 25;
		DropItem "LEG_BOSS_Labolas", 255, 1;
	}
}