class LEG_BaseImp : LEG_BaseMonster
{
	int	flamex;
	int	flamey;
	
	action void A_LEGImpCharge(string VFXActor, bool HeavyCharge)
	{
		A_FaceTarget();
		invoker.AttackCharge ++;
		
		if(LEG_Flames)
		{
			A_SpawnItemEx(VFXActor, 4, bSpriteFlip ? -invoker.flamex : invoker.flamex, invoker.flamey);
			if(invoker.AttackCharge > invoker.AttackThreshold * 0.5) A_SpawnItemEx(VFXActor, 4, bSpriteFlip ? -invoker.flamex : invoker.flamex, invoker.flamey);
			
			if(HeavyCharge)
			{
				A_SpawnItemEx(VFXActor, 4, bSpriteFlip ? -(invoker.flamex * frandom(0.7, 1.3)) : (invoker.flamex * frandom(0.7, 1.3)), invoker.flamey * frandom(1.0, 1.2));
				if(invoker.AttackCharge > invoker.SpecialThreshold * 0.33) A_SpawnItemEx(VFXActor, 4, bSpriteFlip ? -(invoker.flamex * frandom(0.7, 1.3)) : (invoker.flamex * frandom(0.7, 1.3)), invoker.flamey * frandom(1.0, 1.2));
				if(invoker.AttackCharge > invoker.SpecialThreshold * 0.66) A_SpawnItemEx(VFXActor, 4, bSpriteFlip ? -(invoker.flamex * frandom(0.7, 1.3)) : (invoker.flamex * frandom(0.7, 1.3)), invoker.flamey * frandom(1.0, 1.2));
			}
		}
	}
	
	action void A_LEGImpGibb()
	{
		A_LEGIONGibb(bloodRed, bloodSmall);
		
		A_SpawnProjectile("LEG_ImpArm2", 36, -8, randompick(-90, 90), CMF_AIMDIRECTION, frandom(-65, -15));
		A_SpawnProjectile("LEG_ImpLeg", 24, -8, randompick(-90, 90), CMF_AIMDIRECTION, frandom(-65, -15));

		if(LEG_Gore)
		{
			if (!Random(0, 3))
			{
				A_SpawnProjectile("LEG_ImpArm1", 36, 8, randompick(-90, 90), CMF_AIMDIRECTION, frandom(-65, -15));
				A_SpawnProjectile("LEG_ImpLeg", 24, 8, randompick(-90, 90), CMF_AIMDIRECTION, frandom(-65, -15));
			}
			
			for (int i = 0; i < random(1, 3); ++i)
				A_SpawnProjectile("LEG_ImpChunk", frandom(24, 48), frandom(-16, 16), frandom(-45, 45), CMF_AIMDIRECTION, frandom(-65, -15));

			for (int i = 0; i < random(1, 5); ++i)
				A_SpawnProjectile("LEG_ImpSpike", frandom(24, 48), frandom(-16, 16), frandom(-45, 45), CMF_AIMDIRECTION, frandom(-65, -15));
		}
	}

	action void A_LEGImpMelee()
	{
		A_CustomBulletAttack(0, 0, 1, 8, "LEG_MeleePuff", 44, CBAF_AIMFACING);
		A_Recoil(-2);
	}
	
	override void BeginPlay()
	{
		BaseMonster 	= "DoomImp";
		VanillaMonster 	= "LEG_Imp";
		
		AttackThreshold 	= 10;
		SpecialThreshold	= 35;
		SpecialReadyTime	= 175;
		
		flamex		= 21;
		flamey		= 26;
	
		super.BeginPlay();
	}

    Default
    {
        Monster;

        Health 	60;
        Speed 	6;
        Height 	50;
        Radius 	12;
        Mass 	100;
        PainChance 175;

        DamageFactor "Unholy", 	0.5;
        DamageFactor "Holy", 	2.0;
        DamageFactor "Magic", 	0.75;

        Tag         "Imp";
        Species     "Imp";
        SeeSound    "imp/sight";
        PainSound   "imp/pain";
        DeathSound  "imp/death";
        ActiveSound "imp/active";

        +FLOORCLIP;
        +DOHARMSPECIES;
        +MISSILEMORE;
        +JUMPDOWN;
        +FORCEINFIGHTING;
    }
}

// ----------------- //
// -- PROJECTILES -- //
// ----------------- //

class LEG_ImpBallBase : LEG_BaseProj
{
	Default
	{
		Speed 13;
		
		Alpha 0.85;
		Scale 0.25;
		
		LEG_BaseProj.BaseDamage 3;
	}
}

class LEG_ImpBallFire : LEG_ImpBallBase
{
	Default
	{
		DamageType "Fire";
	}

	States
	{
		Spawn:
			LFB1 ABCDEDCB 1 NoDelay
			{
				A_StartSound("Legion/FireBurn", 201, CHANF_LOOPING, 0.25, 1.5);
				A_TailSpawn(3);
			}
			Loop;

		Death:
		XDeath:
		Crash:
			LFB1 F 1
			{
				if(LEG_Flames)
				{
					for (int i = 0; i < 3; ++i)
						A_SpawnItemEx("LEG_ImpTrail", -4, 0, 0, -1, frandom(-2, 2));
					A_SpawnItemEX("LEG_ImpBoom");
				}
				if(LEG_Smoke)
				{
					A_SpawnItemEx("LEG_FireballSmoke");
				}
				A_StartSound("Imp/FireHit", 201);
				A_SetScale(1.0);
				A_SetRenderstyle(0.15, STYLE_ADD);
			}
			LFB1 FF 1 A_SetScale(Scale.X + 0.25);
			Stop;
	}
}

class LEG_ImpBallIce : LEG_ImpBallBase 
{
	Default 
	{
		Speed 10;
		
		Scale 0.30;	
		
		DamageType "Ice";
	}
	
	States 
	{
		Spawn:
			LIB1 A 1 NoDelay A_TailSpawn(4);
			Loop;
			
		Death:
			TNT1 A 0
			{
				A_StartSound("Legion/IceShatter", 201, CHANF_DEFAULT, 1.0, 1.0, 1.25);
					
				if(LEG_Flames)
				{
					for (int i = 0; i < 5; ++i)
						A_SpawnItemEx("LEG_VERYSMOLIceChunk", 0, 0, 0, frandom(-10, 10), frandom(-10, 10), frandom(0, 12));
					for (int i = 0; i < 2; ++i)
						A_SpawnItemEx("LEG_SmolIceChunk", 0, 0, 0, frandom(-3, 0), frandom(-3, 3), frandom(0, 3));
				}
				
				if(LEG_Smoke)
				{
					for (int i = 0; i < 4; ++i)
						 A_SpawnItemEx("LEG_CacoIceMist", 0, 0, 0, frandom(-3, 0), frandom(-3, 3), frandom(-3, 3));
				}
				
				for (int i = 0; i < 2; ++i)
					A_SpawnItemEx("LEG_CacoIceMist");
			}
			Stop;
	}
}

class LEG_ImpBallLightning : LEG_ImpBallBase
{
	Default
	{
		Speed 16;
		
		Alpha 1.0;
		Scale 0.35;

		DamageType "Electric";
	}

	States
	{
		Spawn:
			HEFX AB random(1, 3) NoDelay
			{
				A_StartSound("Legion/BoltZap", 201, CHANF_LOOPING, 1.0, 1.0);
				A_SetRoll(frandom(0, 360));
				bSpriteFlip = random(0, 1);
				A_TailSpawn(6);
			}
			Loop;
			
		Death:
		XDeath:
		Crash:
			HEFX C 1
			{
				A_StartSound("Imp/BoltHit", 201);
				A_SpawnItemEx("LEG_BodyBolt", 0, frandom(-12, 12), frandom(-12, 12));
				A_SetScale(1.0);
			}
			HEFX DEFGH 1;
			TNT1 AAAAA random(3, 24)
			{
				if(LEG_FLames)
				{
					A_SpawnItemEx("LEG_BodyBolt", 0, frandom(-12, 12), frandom(-12, 12));
				}
			}
			Stop;
    }
}

class LEG_ImpBallSoul : LEG_ImpBallBase
{
    Default
    {
		Scale 0.35;
		Alpha 0.66;
		
		DamageFunction 	5 * random(1, 8);
		Speed 	14;
		DamageType "Unholy";
		
		LEG_BaseProj.VFXType "LEG_SoulFlame";
    }
	
	States 
	{
		Spawn:
			LFB9 A 1 NoDelay
			{
				A_TailSpawn(8);
				A_SetRoll(Roll + 15);
			}
			Loop;
			
		Death:
		XDeath:
		Crash:
			LFB9 B 1
			{
				Alpha = 0.30;
				A_StartSound("Imp/GraveHit", 201);
				A_SetScale(1.0);
				if(LEG_Flames)
				{
					for (int i = 0; i < random(1, 3); ++i)
					{
						A_SpawnItemEx("LEG_SoulFlame", 0, 0, 0, 0, frandom(-1, 1), frandom(0, 1));
						A_SpawnItemEX("LEG_SoulFlame", 0, frandom(-8, 8), frandom(-8, 8));
					}
				}
			}
			LFB9 BBB 1 A_SetScale(Scale.X + 0.25);
			Stop;
	}
}

class LEG_ImpBallPlasma : LEG_ImpBallBase
{
	Default
	{
		Alpha 1.0;
		Scale 0.5;
		
		DamageFunction 	4 * random(1, 8);
		Speed 	20;

		DamageType "Plasma";
	}

    States
    {
        Spawn:
            LFB2 A 1 A_StartSound("Imp/PlasmaFly", 201, CHANF_LOOPING, 0.25, 1.5);
            Loop;

        Death:
        XDeath:
        Crash:
          LFB2 B 1
          {
            A_StartSound("Imp/PlasmaHit", 201);
            A_SetScale(1.0);
            A_SetRenderstyle(0.15, STYLE_ADD);
            if(LEG_Flames)
            {
              for (int i = 0; i < 2; ++i)
		            A_SpawnItemEx("LEG_PlasmaFlame", 0, 0, 0, -2, frandom(-1, 1), frandom(0, 1));
            }

            if(LEG_Smoke)
            {
              A_SpawnItemEx("LEG_FireballSmoke");
            }
          }
          LFB2 BB 1 A_SetScale(Scale.X + 0.35);
          Stop;
    }
}

// ----------- //
// -- GIBBS -- //
// ----------- //

class LEG_BaseImpGibb : LEGBaseGibb
{
	Default
	{
		Speed   14;
	}
}

class LEG_ImpLeg : LEG_BaseImpGibb
{
	States
	{
		Spawn:
			IGIB A 1 NoDelay A_LEGBloodSpray("LEG_LimbMist");
			Loop;
	}
}

class LEG_ImpArm1 : LEG_BaseImpGibb
{
	States
	{
		Spawn:
			IGIB B 1 NoDelay A_LEGBloodSpray("LEG_LimbMist");
			Loop;
	}
}

class LEG_ImpSpike : LEG_BaseImpGibb
{
	States
	{
		Spawn:
			IGIB C 1 NoDelay A_LEGBloodSpray("LEG_LimbMist");
			Loop;
	}
}

class LEG_ImpChunk : LEG_BaseImpGibb
{
	States
	{
		Spawn:
			IGIB D 1 NoDelay A_LEGBloodSpray("LEG_LimbMist");
			Loop;
	}
}

class LEG_ImpArm2 : LEG_BaseImpGibb
{
	States
	{
		Spawn:
		IGIB E 1 NoDelay A_LEGBloodSpray("LEG_LimbMist");
		Loop;
	}
}

class ImpSpawner : RandomSpawner replaces DoomImp
{
	Default
	{
		DropItem "LEG_Imp",     	 255, 100;
		DropItem "LEG_DarkImp", 	 255, 36;
		DropItem "LEG_IceImp",  	 255, 34;
		DropItem "LEG_CyberImp",     255, 14;
		DropItem "LEG_Harvester",	 255, 14;	
		DropItem "LEG_BOSS_ImpMage", 255, 1;
	}
}

class LEG_DeadImp : Actor replaces DeadDoomImp
{
	Default 
	{
		+NOINTERACTION;
	}
	
	States 
	{
		Spawn:
			TNT1 A 0 NoDelay { bSPRITEFLIP = random(0, 1); }
			TNT1 A 0 A_Jump(256, "Reg1", "Reg2", "Reg3", "Dark1", "Dark2", "Ice1", "Ice2", "Mage");
			
		Reg1:
			TROO M -1;
			Stop;
		Reg2:
			TRO3 M -1;
			Stop;
		Reg3:
			TRO4 M -1;
			Stop;
			
		Dark1:
			DROO M -1;
			Stop;
		Dark2:
			DRO3 M -1;
			Stop;	
		
		Ice1:
			FROO M -1;
			Stop;
		Ice2:
			FRO4 M -1;
			Stop;
			
		Mage:
			TRO2 M -1;
			Stop;
	}
}