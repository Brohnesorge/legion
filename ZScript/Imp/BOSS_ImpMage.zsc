class LEG_BOSS_ImpMage : LEG_BaseImp
{
	override void BeginPlay()
	{
		legAltRace 	= TRUE;
		legBoss		= TRUE;
		WarnMessage	= "Smells like sulfer and quicksilver...";
	
		super.BeginPlay();
	}
	
	override void PostBeginPlay()
	{	
		AttackThreshold 	= 32; //Fire
		Attack2Threshold 	= 60; //Ice
		Attack3Threshold 	= 24; //Bolt
		SpecialThreshold	= 20;
		SpecialReadyTime	= 87;
		
		flamex		= 28;
		flamey		= 32;

		Super.PostBeginPlay();	
	}
	
	action void A_LEGBossImpFire()
	{
		for (int i = -15; i < 15; i += 10)
			A_SpawnProjectile("LEG_BossImpFireBall", 32, 0, i);
		A_SpawnProjectile("LEG_BossImpFireBallBig");
		
		A_StartSound("Imp/FireCast", 200);
		invoker.AttackCharge = 0;
	}

	action void A_LEGBossImpLightning()
	{
		A_SpawnItemEX("LEG_ImpBossHandBolt", 4, 2, 34);
		A_CustomBulletAttack(0, 0, 1, 32, "LEG_LightningPuff", 0, CBAF_NORANDOMPUFFZ | CBAF_NORANDOM | CBAF_AIMFACING);
		A_StartSound("Imp/BoltCast", 200);
		invoker.AttackCharge = 0;
	}

	action void A_LEGBossImpBaron()
	{
		A_SpawnProjectile("LEG_BaronBall");
		A_StartSound("Baron/Cast", 200);
		invoker.AttackCharge = 0;
		invoker.SpecialTimer = 0;
	}

	Default
	{
		Health          500;
		Speed           7;
		Scale           1.1;
		Height          56;
		Radius          16;
		Mass            250;
		PainChance      75;
		MeleeRange      32;
		MeleeThreshold  32;

		DamageFactor "Unholy", 0.5;
		DamageFactor "Holy",   2.0;
		DamageFactor "Magic",  0.35;
		DamageFactor "Fire", 0.5;
		DamageFactor "Ice", 0.5;
		DamageFactor "Electric", 0.5;

		Tag         "Imp Magi";
		Obituary    "Burned, shocked and frozen by an imp";

		SeeSound    "ImpMage/sight";
		PainSound   "ImpMage/pain";
		DeathSound  "ImpMage/death";
		ActiveSound "ImpMage/active";
		AttackSound "ImpMage/attack";

		+AVOIDMELEE;
		+QUICKTORETALIATE;
	}

	States
	{
		Spawn:
			TRO2 B 5 A_Look;
			Loop;

		See:
			TRO2 AABBCCDD 2 A_Chase;
			Loop;

		Missile:
			TRO2 EEE 2 A_FaceTarget();
			TNT1 A 0 A_JumpIf(!LEG_Spec, "FlameOn");
			TNT1 A 0 A_Jump(256, "FlameOn", "TwoInThePinkOneInTheStink", "IceManCometh", "HeThinksHesPeople");
			
		// - Fire - //
		FlameOn:
			TNT1 A 0 A_JumpIf(self.AttackCharge >= self.AttackThreshold, "FlameWar");
			TRO2 E 1 A_LEGImpCharge("LEG_ImpTrail", FALSE);
			Loop;
		FlameWar:
			TRO2 F 1;
			TRO2 G 8 A_LEGBossImpFire();
			TRO2 FE 2;
			Goto See;
			
		// - Ice - //
		IceManCometh:
			TNT1 A 0 A_JumpIf(self.AttackCharge >= self.Attack2Threshold, "FreezeWar");
			TRO2 E 1 
			{
				A_LEGImpCharge("LEG_IceMistSmall", FALSE);
				A_VileTarget("MageImpIce");
			}
			Loop;
		FreezeWar:
			TRO2 F 1{ invoker.AttackCharge = 0; A_FaceTarget();}
			TRO2 G 13
			{
				//A_VileAttack("ImpMage/IceCast", random(1, 64), 0, 0, 0.0, "Ice", VAF_DMGTYPEAPPLYTODIRECT);
				A_StartSound("ImpMage/IceCast", 198);
				A_CustomBulletAttack(0, 0, 1, random(1, 40), "MageImpIceBurst", 0, CBAF_NORANDOM);
				A_VileTarget("MageImpIceBurst");
			}
			TRO2 FE 4;
			Goto See;
			
		// - Lightning - //	
		TwoInThePinkOneInTheStink:
			TNT1 A 0 A_JumpIf(self.AttackCharge >= self.Attack3Threshold, "BoltWar");
			TRO2 E 1 A_LEGImpCharge("LEG_BoltArc", FALSE);
			Loop;
			
		BoltWar:
			TRO2 F 2;
			TRO2 G 6 A_LEGBossImpLightning();
			TRO2 FE 1;
			Goto See;
			
		// - Special Magic - //	
		HeThinksHesPeople:
			TNT1 A 0 A_JumpIf(self.SpecialTimer < self.SpecialReadyTime || !LEG_Spec, "Missile");
		MagiCharge:
			TNT1 A 0 A_JumpIf(self.AttackCharge >= self.SpecialThreshold, "MagiWar");
			TRO2 E 1 A_LEGImpCharge("LEG_BaronFlame", TRUE);
			Loop;
			
		MagiWar:
			TRO2 F 1;
			TRO2 G 7 A_LEGBossImpBaron();
			TRO2 FE 1;
			TRO2 F 1 A_FaceTarget();
			TRO2 G 7 A_LEGBossImpBaron();
			TRO2 FE 1;
			TRO2 F 1 A_FaceTarget();
			TRO2 G 7 A_LEGBossImpBaron();
			TRO2 FE 1;
			Goto See;

		Pain:
			TRO2 H 2;
			TRO2 H 6 A_LEGPain;
			Goto See;

		Death:
			TRO2 HHHHHHHHHHHHHH random(1, 3)
			{
				A_SpawnItemEx("LEG_BodyBolt", 0, frandom(-18, 18), frandom(0, 48));
				
				for (int i = 0; i < random(0, 6); ++i)
				{
					A_SpawnItemEx("LEG_GreenImpTrail", 0, frandom(-18, 18), frandom(0, 48));
					A_SpawnItemEx("LEG_ImpTrail", 0, frandom(-18, 18), frandom(0, 48));	
				}
			}
			TRO2 H 10;
			TRO2 III 1
			{
				for (int i = 0; i < random(6, 12); ++i)
					A_SpawnItemEx("LEG_BBFlame", frandom(-9, 9), frandom(-9, 9), frandom(24, 48), 0, frandom(-4, 4), frandom(3, 12));
			}
			TRO2 J 3 A_ScreamAndUnblock;
			TRO2 KL 3;
			TRO2 M -1 A_StartSound("General/BodyThump", 198);
			Stop;

		Raise:
			Stop;
	}
}

class LEG_BossImpFireBall : LEG_ImpBallFire
{
	Default
	{
		Speed 	13;
		
		Scale 	0.15;

		LEG_BaseProj.BaseDamage 1;

		+SEEKERMISSILE;
	}

	States
	{
		Spawn:
			LFB1 ABCDEDCB 1;
		Homing:
			LFB1 ABCDEDCB 1
			{
				A_StartSound("Legion/FireBurn", 201, CHANF_LOOPING, 0.25, 1.5);
				A_SeekerMissile(0, 5);
				A_TailSpawn(4);
			}
		Loop;
	}
}

class LEG_BossImpFireBallBig : LEG_ImpBallFireBig
{
	Default 
	{
		+LEG_BaseProj.CAUSESTATUS;
	}
}

class LEG_BossImpLightningPuff : Actor
{
	Default
	{
		Projectile;

		Scale 1.0;
		Alpha 1.0;

		RenderStyle "Add";
		DamageType "Electric";

		+NOINTERACTION;
		+PUFFONACTORS;
		+BLOODLESSIMPACT;
		+EXTREMEDEATH;
		+ROLLSPRITE;
		+BRIGHT;
	}

	States
	{
		Spawn:
		Crash:
		XDeath:
			TNT1 A 0 NoDelay A_StartSound("Imp/BoltHit", 201);
			LNAC BBBB 1
			{
				invoker.bSpriteFlip = random(0, 1);
				A_SetRoll(random(0, 360));
				A_SetScale(Scale.X + frandom(-0.33, 0.33));
			}
			TNT1 AAAAAAAAAAA random(4, 105)
			{
				if(LEG_FLames)
				{
					A_SpawnItemEx("LEG_BodyBolt", 0, frandom(-12, 12), frandom(-12, 12));
				}
			}
			Stop;
	}
}

class LEG_ImpBossHandBolt : Actor
{
	Default
	{
		Projectile;

		Scale 0.33;
		Alpha 1.0;

		Renderstyle "Add";

		+NOINTERACTION;
		+ROLLSPRITE;
		+BRIGHT;
	}

	States
	{
		Spawn:
			LNAC BBBBB 1 NoDelay
			{
				invoker.bSpriteFlip = random(0, 1);
				A_SetRoll(random(0, 360));
				A_SetScale(Scale.X + frandom(-0.1, 0.1));
			}
			Stop;
	}
}

class MageImpIce : LEG_BaseArchFocus
{
	States 
	{
		Spawn:
			TNT1 A 0 NoDelay
			{
				self.FocusVFX 	= "LEG_IceMistSmall";
				self.VFXWide	= 16;
			}
			TNT1 A 1 A_Jump(256, "SpawningTheShit");
			Stop;
	}
}

class MageImpIceBurst : LEG_ArchBoom
{
	States
	{
		XDeath:
			TNT1 A 0
			{
				A_StartSound("ImpMage/IceBurst", 201);
				if(LEG_Extra) A_RadiusGive("LEG_FrozenGiver", 24, RGF_PLAYERS | RGF_NOSIGHT);
			}
			LICE ABCDEFGH 1 A_Fire(0);
			LICE H 35;
			TNT1 A 0 A_StartSound("Legion/IceShatter", 201);
			LICE IJKLMNOPQRS 1;
			Stop;
	}
}
