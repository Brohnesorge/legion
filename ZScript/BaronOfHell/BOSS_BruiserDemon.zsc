class LEG_BOSS_BruiserDemon : LEG_BaseBaron
{
	override void BeginPlay()
	{
		legAltRace 	= TRUE;
		legBoss		= TRUE;
		WarnMessage	= "The air feels like its burning...";
	
		super.BeginPlay();
	}

	override void PostBeginPlay()
	{
		if(LEG_Extra) { A_GiveInventory("LEG_BruiserHealing", 1); }
		
		AttackThreshold 	= 13;
		Attack2Threshold  	= 56;
		SpecialReadyTime	= 175;
		SpecialThreshold	= 50;
		
		flamex = 38;
		flamey = 76;
		
		super.PostBeginPlay();
	}

	action void A_LEGBruiserMelee()
	{
		A_CustomBulletAttack(0, 0, 1, 10 * random(1, 8), "LEG_BaronPuff", 64, CBAF_AIMFACING | CBAF_NORANDOM);
		A_CustomBulletAttack(0, 0, 1, 10 * random(1, 4), "LEG_HellFirePuff", 64, CBAF_AIMFACING | CBAF_NORANDOM);
		A_ClearCounter();
	}
	
	action void A_ThrowBruiserBall()
	{
		A_SpawnProjectile("LEG_BruiserBall");
		A_StartSound("Bruiser/Cast", 200);
		invoker.AttackCharge = 0;
		invoker.SpecialTimer ++;
		A_ClearCounter();
	}
	
	action void A_ThrowPyroclasm()
	{
		A_SpawnProjectile("LEG_ExBruiserBall");
		A_StartSound("Bruiser/Hellfire", 201, CHANF_DEFAULT, 1.0, 0.25);
		invoker.AttackCharge = 0;
		invoker.SpecialTimer = 0;
		A_ClearCounter();
	}
	
	override void tick()
	{
		if(self.Health > 0)
		{
			for (int i = 0; i < random(1, 3); ++i)
				A_SpawnItemEx("LEG_BigFire", frandom(-24, 24), frandom(-24, 24), frandom(0, 76), 0, 0, frandom(0, 2));
		}

		super.tick();
	}

	Default
	{
		Health 		4000;
		Speed 		12;
		PainChance 	35;
		
		Scale 1.3;

		DamageFactor "Unholy", 0.5;
		DamageFactor "Holy", 2.0;
		DamageFactor "Magic", 0.5;
		DamageFactor "Fire", 0.0;
		DamageFactor "HellFire", 0.25;
		DamageFactor "Ice", 0.0;
		DamageFactor "Electric", 0.5;
		DamageFactor "SmallArms", 0.5;
		DamageFactor "Bullet", 0.75;
		DamageFactor "PLWater", 3.0;

		SeeSound    "bruiser/see";
		PainSound   "bruiser/pain";
		DeathSound  "bruiser/death";
		ActiveSound "bruiser/active";
		BloodColor  "Yellow";
		
		Tag "Bruiser Demon";
		
		+MISSILEMORE;
	}

	States
	{
		Spawn:
			BRUS B 10 A_Look;
			Loop;

		See:
			TNT1 A 0 
			{
				A_StartSound("Baron/FootStep", 201, CHANF_DEFAULT, 1.0, 1.0, frandom(0.85, 1.15));
				for (int i = 0; i < random(1, 3); ++i)
					A_SpawnItemEX("LEG_BruiserStepFire", 0, 12 + random(-4, 4));
			}
			BRUS AA 4 A_Chase("Melee", "Missile", CHF_DONTMOVE);
			BRUS BBBB 2 A_Chase;
			TNT1 A 0 
			{
				A_StartSound("Baron/FootStep", 201, CHANF_DEFAULT, 1.0, 1.0, frandom(0.85, 1.15));
				for (int i = 0; i < random(1, 3); ++i)
					A_SpawnItemEX("LEG_BruiserStepFire", 0, -12 + random(-4, 4));
			}
			BRUS CC 4 A_Chase("Melee", "Missile", CHF_DONTMOVE);
			BRUS DDDD 2 A_Chase;
			Loop;

		Melee:
			TNT1 A 0 { bSpriteFlip = random(0, 1); }
			BRUS EEEEE 2 A_FaceTarget();
			BRUS F 2;
			BRUS G 10 A_LEGBruiserMelee();
			Goto See;

		Missile:
			TNT1 A 0 A_JumpIfTargetInsideMeleeRange("Melee");
			TNT1 A 0 A_JumpIf(self.CounterCharge > 20, "Pyroclasm");
			TNT1 A 0 A_Jump(128, "Pyroclasm");
			
		ThrowNormal:
				TNT1 A 0 { bSpriteFlip = random(0, 1); }
				BRUS FE 1 A_FaceTarget();
			
			ChargeNormal:
				TNT1 A 0 A_JumpIf(self.AttackCharge >= self.AttackThreshold, "ActualThrow");
				BRUS E 1 
				{
					A_LEGBBcharge("LEG_BruiserFlame", "LEG_BruiserFlame");
					A_SpawnItemEX("LEG_BruiserStepFire", random(-36, 36), random(-36, 36));
				}
				Loop;
			
		ActualThrow:
			BRUS F 1;
			BRUS G 6 A_ThrowBruiserBall();
			Goto See;
			
		Pyroclasm:
			TNT1 A 0 A_JumpIf(self.SpecialTimer < self.SpecialReadyTime || !LEG_Spec, "ThrowNormal");
			TNT1 A 0 A_Jump(128, "Firestorm");
			BRUS MLK 3 A_FaceTarget;
			
		ChargeBalls:
			TNT1 A 0 A_JumpIf(self.AttackCharge >= self.SpecialThreshold, "ThrowClasm");
			BRUS K 1
			{
				A_LEGFanCharge("LEG_BruiserFlame", "LEG_BruiserFlame");
				A_SpawnItemEx("ChargingBruiserBall", 6, self.FlameX, FlameY);
				A_SpawnItemEx("ChargingBruiserBall", 6, -self.FlameX, FlameY);
				A_SpawnItemEX("LEG_BruiserStepFire", random(-48, 48), random(-48, 48));
			}
			Loop;
			
		ThrowClasm:
			BRUS K 2 A_StartSound("bruiser/see", 198, CHANF_DEFAULT, 1.0, ATTN_NONE, 1.25);
			BRUS L 2;
			BRUS GFE 2 A_FaceTarget();
			BRUS EF 2;
			BRUS G 6 A_ThrowPyroclasm();
			BRUS HI 2;
			BRUS J 6 A_ThrowPyroclasm();
			Goto See;
			
		Firestorm:
			TNT1 A 0 A_CheckRange(256, "Pyroclasm");
			BRUS KLM 3;
			
		ChargeStorm:
			TNT1 A 0 A_JumpIf(self.AttackCharge >= self.SpecialThreshold * 1.25, "IAmTheStormThatIsApproaching");
			BRUS M 1
			{
				A_SpawnItemEx("LEG_BruiserPillar", 32 * frandom(-self.AttackCharge, self.AttackCharge), 0, 0, 0, 0, 0, frandom(0, 360), SXF_SETMASTER);
				if(self.AttackCharge > self.SpecialThreshold * 0.33) A_SpawnItemEx("LEG_BruiserPillar", 8 * frandom(-self.AttackCharge, self.AttackCharge), 0, 0, 0, 0, 0, frandom(0, 360), SXF_SETMASTER);
				if(self.AttackCharge > self.SpecialThreshold * 0.66) A_SpawnItemEx("LEG_BruiserPillar", 8 * frandom(-self.AttackCharge, self.AttackCharge), 0, 0, 0, 0, 0, frandom(0, 360), SXF_SETMASTER);
				
				A_SpawnItemEX("LEG_BruiserStepFire", random(-128, 128), random(-128, 128));
				self.AttackCharge ++;
				
				if(LEG_Flames)
				{
					A_SpawnItemEx("LEG_BigFire", frandom(-24, 24), frandom(-24, 24), frandom(0, 76), 0, 0, frandom(0, 2));
					if(self.AttackCharge > self.SpecialThreshold * 0.5) A_SpawnItemEx("LEG_BigFire", frandom(-48, 48), frandom(-48, 48), frandom(0, 96), 0, 0, frandom(0, 2));
				}
			}
			Loop;
			
		IAmTheStormThatIsApproaching:
			BRUS M 2 A_StartSound("bruiser/see", 198, CHANF_DEFAULT, 1.0, ATTN_NONE, 0.75);
			BRUS L 2;
			BRUS K 2
			{
				A_StartSound("Bruiser/Hellfire", 201);
				A_GiveToChildren("BoomNow", 1);
				A_ClearCounter();
				self.SpecialTimer = 0;
				self.AttackCharge = 0;
			}
			BRUS KKKKKKKK 2 
			{
				A_SpawnItemEX("LEG_BruiserStepFire", random(-128, 128), random(-128, 128));
				A_SpawnItemEX("LEG_BigFire", random(-128, 128), random(-128, 128));
			}
			BRUS K 70;
			Goto See;

		Pain:
			BRUS N 4
			{
				A_RemoveChildren(TRUE);
				A_StopAllSounds();
			}
			BRUS N 3 A_BaronPain();
			TNT1 A 0 A_JumpIf(self.CounterAttack == TRUE, "Missile");
			Goto See;
		
		Raise:
			Stop;

		Death:
			TNT1 A 0
			{
				A_RemoveChildren(TRUE);
				A_StopAllSounds();
			}
			BRUS NNNNNNNNNNNN 3 
			{
				for (int i = 0; i < random(0, 3); ++i)
					A_SpawnItemEx("LEG_BoomPlume", 0, frandom(-12, 12), frandom(48, 64), 0, frandom(-2, 2), random(3, 6));
			}
			BRUS NNNNNNNNNNNN 3 
			{
				for (int i = 0; i < random(0, 3); ++i)
					A_SpawnItemEx("LEG_BBBFlame", frandom(-12, 12), frandom(-12, 12), frandom(0, 64), 0, 0, frandom(0, 2));
				for (int i = 0; i < random(0, 3); ++i)
					A_SpawnItemEx("LEG_BoomPlume", 0, frandom(-12, 12), frandom(48, 64), 0, frandom(-2, 2), random(3, 6));
			}
			TNT1 A 0
			{
				A_ScreamAndUnblock();
				A_Quake(4, 35, 0, 720);
				
				for (int i = 0; i < 3; ++i)
					A_SpawnItemEx("LEG_FireWave", 0, 0, 48);
			}
			BRUD ABCDEFGHIJKLMNOPQR 3
			{
				for (int i = 0; i < random(3, 10); ++i)
				{
					A_SpawnItemEx("LEG_BigFire", frandom(-48, 48), frandom(-48, 48), frandom(0, 76), 0, 0, frandom(0, 2));
					A_SpawnItemEX("LEG_BruiserStepFire", random(-128, 128), random(-128, 128));
				}
			}
			TNT1 A 0  A_StartSound("General/BigBoiFall", 201);
			BRUD STUVW 4;
			BRUD W -1;
			Stop;
	}
}

class LEG_BruiserHealing : PowerRegeneration
{
	Default
	{
		Powerup.Duration 0x7FFFFFFF;
		Powerup.Strength 5;
		+INVENTORY.PERSISTENTPOWER
		+INVENTORY.UNDROPPABLE
		+INVENTORY.AUTOACTIVATE
	}

}

class LEG_BruiserStepFire : Actor 
{
	override void tick()
	{
		if(!random(0, 1))
		{
			A_SpawnItemEx("LEG_IMpTrail");
		}

		if(!random(0, 3))
		{
			A_Explode(1, 8, XF_EXPLICITDAMAGETYPE, FALSE, 8, 0, 0, "LEG_NullPuff", "Fire"); 
		}

		super.tick();
	}


	Default 
	{
		+MOVEWITHSECTOR;
		+NOCLIP;
		+NODAMAGETHRUST;
		+FLOORHUGGER;
	}
	
	States 
	{
		Spawn:
			TNT1 AAAAA 35;
			Stop;
	}
}

class LEG_BruiserBall : LEG_BaronBall
{
	action void A_FlameExpand(float ScalePlus, int BoomRadi, int BoomCount)
	{
		A_SetScale(Scale.X + ScalePlus);
		if(LEG_Flames)
		{
				
			for (int i = 0; i < BoomCount; ++i)
				A_SpawnItemEX("LEG_ImpBoom", -(BoomRadi * 0.5), random(-BoomRadi, BoomRadi), random(-BoomRadi, BoomRadi));
		}
	}

	Default
	{
		Speed 	25;
		
        DamageType "HellFire";
		
		LEG_BaseProj.BaseDamage 10;
		LEG_BaseProj.VFXtype "LEG_BruiserFlame";
	}

	States
	{
		Spawn:
			LFB1 ABCDEDCB 1 NoDelay
			{
				A_StartSound("Legion/FireBurn", 201, CHANF_LOOPING, 0.45, 1.0, 0.75);
				A_TailSpawn(6);
				A_TailSpawn(6);
				
				if(LEG_Smoke)
				{
					A_SpawnItemEx("LEG_FireballSmoke", 0, 0, 0, 0, 0, random(0, 2));
				}
			}
			Loop;

		Death:
		XDeath:
		Crash:
			LFB1 F 1
			{
				if(LEG_Flames)
				{
					A_SpawnItemEx("LEG_FireWave");
				
					for (int i = 0; i < 6; ++i)
						A_SpawnItemEx("LEG_BBBFlame", 0, 0, 0, -1, frandom(-3, 3));
						
					for (int i = 0; i < 2; ++i)
						A_SpawnItemEX("LEG_ImpBoom", -4, random(-4, 4), random(-4, 4));
				}
				if(LEG_Smoke)
				{
					A_SpawnItemEx("LEG_FireballSmoke");
					
					for (int i = 0; i < 3; ++i)
						A_SpawnItemEx("LEG_FireballSmoke", frandom(-10, 10), frandom(-10, 10), frandom(-5, 15), 0, 0, frandom(0, 2));
				}
				A_Explode(32, 32, 0, TRUE, 0, 0, 0, "BulletPuff", "Fire");
				A_StartSound("Baron/BallHit", 201, CHANF_DEFAULT, 1.0, 1.0, 1.25);
				A_SetScale(1.0);
				A_SetRenderstyle(0.15, STYLE_ADD);
			}
			LFB1 FF 1 
			{
				A_FlameExpand(0.25, 4, 2);
				if(LEG_Flames)
				{
					for (int i = 0; i < 6; ++i)
						A_SpawnItemEx("LEG_BBBFlame", 0, 0, 0, -1, frandom(-3, 3), random(0, 4));
				}
			}
			TNT1 AA 1 A_FlameExpand(0.25, 8, 4);
			TNT1 AA 1 A_FlameExpand(0.25, 16, 8);
			TNT1 AA 1 A_FlameExpand(0.25, 24, 8);
			TNT1 AAAAAAAA 6 { if(LEG_Smoke) A_SpawnItemEx("LEG_FireballSmoke", -4, frandom(-6, 6), frandom(0, 3), 0, 0, random(1, 2)); }
			Stop;
	}
}

class LEG_BruiserPillar : Actor
{
	override void BeginPlay()
	{
		Roll 		= random(0, 360);
		bSpriteFlip	= random(0, 1);
		
		super.BeginPlay();
	}

	Default 
	{
		Alpha 	0.0;
		Scale 	0.1;
		Gravity 100;
		
		Renderstyle "Add";
	
		+BRIGHT;
		+FLATSPRITE;
		+FLOORHUGGER;
		+MOVEWITHSECTOR;
		+RELATIVETOFLOOR;
		+ROLLSPRITE;
		+ROLLCENTER;
		+NOCLIP;
		+THRUACTORS;
	}
	States
	{
		Spawn:
			LGXB A 1 NoDelay 
			{
				A_FadeTo(1.0, 0.05, FALSE);
				A_SetScale(Scale.X + 0.05);
				
			}
			LGXB A 0 A_JumpIf(self.Alpha >= 1.0, "WaitForBoom");
			Loop;
			
		WaitForBoom:
			LGXB A 1 
			{
				A_SpawnItemEx("LEG_FireballSmoke", 0, 0, random(0, 6), 0, 0, random(1, 2));
				A_SpawnItemEx("LEG_ImpTrail", random(-2, 2), random(-2, 2), 0, 0, 0, random(0, 1));
			}
			LGXB A 1 A_JumpIfInventory("BoomNow", 1, "Death");
			Loop;

		Death:
			LGXB AAAAAAAAAA random(0, 2) A_SpawnItemEx("LEG_ImpTrail", frandom(-6, 6), frandom(-6, 6), 0, 0, 0, frandom(2, 6));
			TNT1 A 1 
			{
				A_Explode(64, 64, XF_THRUSTZ | XF_EXPLICITDAMAGETYPE, FALSE, 16, 0, 0, "LEG_NullPuff", "HellFire");
				if(LEG_Status) A_RadiusGive("LEG_HellfireBurner", 64, RGF_PLAYERS | RGF_MONSTERS | RGF_NOTARGET, 1);
				
				A_StartSound("Bruiser/PillarBoom", 201, CHANF_DEFAULT);
				
				A_SpawnItemEx("LEG_BruiserFountianFlame", frandom(-12, 12), frandom(-12, 12), 0, 0, 0, frandom(6, 12));
				A_SpawnItemEx("LEG_FireWave");
				
				if(LEG_Smoke)
				{
					A_SpawnItemEx("LEG_FireballSmoke");
					A_SpawnItemEx("LEG_FireballSmoke", frandom(-5, 5), frandom(-5, 5), frandom(-5,5), 0, frandom(-3, 3), frandom(0,6));
					A_SpawnItemEx("LEG_FireballSmoke", frandom(-5, 5), frandom(-5, 5), frandom(-5,5), 0, frandom(-3, 3), frandom(0,6));
				}
			}
			TNT1 AAAAA 2 A_SpawnItemEx("LEG_BruiserFountianFlame", frandom(-12, 12), frandom(-12, 12), 0, 0, 0, frandom(6, 12));
			TNT1 AAAAAAAAAA 1 A_SpawnItemEx("LEG_ImpTrail", frandom(-6, 6), frandom(-6, 6), 0, 0, 0, frandom(2, 6));
			TNT1 AAAAAAAAAA 2 A_SpawnItemEx("LEG_ImpTrail", frandom(-6, 6), frandom(-6, 6), 0, 0, 0, frandom(2, 6));
			TNT1 AAAAAAAAAA 2 A_SpawnItemEx("LEG_ImpTrail", frandom(-6, 6), frandom(-6, 6), 0, 0, 0, frandom(2, 6));
			TNT1 AAAAAAAAAA 4 
			{
				if(LEG_Smoke)
				{
					for (int i = 0; i < random(0, 2); ++i)
						A_SpawnItemEx("LEG_FireballSmoke", frandom(-16, 16), frandom(-16, 16), frandom(0, 16), 0, 0, random(0, 2));
				}
			}
			Stop;
	}
}

class BoomNow : Inventory {}

class LEG_BruiserFountianFlame : Actor
{
	Default
	{
		+NoInteraction
		Renderstyle "Add";
		Alpha 0.65;
		Scale 2.0;
	}

	States
	{
		Spawn:
			LGXA HIJKMNO 3 NoDelay Bright;
			Stop;
	}

}

class LEG_ExBruiserBall : LEG_BruiserBall
{
	Default
	{
		Speed 	28;

		Renderstyle "Normal";
		
		Alpha 1.0;
		Scale 1.0;
		
		LEG_BaseProj.BaseDamage 15;
	}

	States
	{
		Spawn:
			LFB] A 1 NoDelay
			{
				A_StartSound("Legion/FireBurn", 201, CHANF_LOOPING, 0.45, 1.0, 0.75);
				if(LEG_Flames)
				{
					A_SpawnItemEx("LEG_BBBFlame", -8, frandom(-6, 6), frandom(-6, 6));
					
					for (int i = 0; i < 3; ++i)
						A_SpawnItemEx("LEG_RevTrail", -4, frandom(-6, 6), frandom(-6, 6));
					for (int i = 0; i < 2; ++i)
						A_SpawnItemEx("LEG_RevTrail", -4, frandom(-12, 12), frandom(-12, 12));
				}
				
				if(LEG_Smoke)
				{
					A_SpawnItemEx("LEG_FireballSmoke", 0, 0, 0, 0, 0, random(0, 2));
				}
			}
			Loop;

		Death:
		XDeath:
		Crash:
			TNT1 A 1
			{
				A_Explode(192, 192, XF_THRUSTZ | XF_EXPLICITDAMAGETYPE, FALSE, 0, 0, 0, "LEG_NullPuff", "HellFire");
				
				if(LEG_Status) A_RadiusGive("LEG_HellfireBurner", 192, RGF_PLAYERS | RGF_MONSTERS | RGF_NOTARGET, 1);
				
				A_StartSound("Bruiser/PyroBoom", 202, CHANF_DEFAULT, 1.0, 0.65);
				A_StartSound("Legion/FarExplosion", 201, CHANF_DEFAULT, 1.0, ATTN_NONE, 0.85);
				
				A_SpawnItemEx("LEG_Boom");
				
				if(LEG_Flames)
				{
					A_SpawnItemEx("LEG_FireWave");
					A_SpawnItemEx("LEG_BoomWave");
				
					for (int i = 0; i < 3; ++i)
						A_SpawnItemEx("LEG_Boom", frandom(-15, 15), frandom(-15, 15), frandom(-10, 10));
						
					for (int i = 0; i < 3; ++i)
						A_SpawnItemEx("LEG_BoomPlume", 0, 0, 0, 0, 0, frandom(3, 12));
					
					for (int i = 0; i < 3; ++i)
						A_SpawnItemEx("LEG_BBBFlame", 0, 0, 0, -1, frandom(-6, 6));
				}
				
				if(LEG_Smoke)
				{
					for (int i = 0; i < random(2, 8); ++i)
						A_SpawnItemEx("LEG_ExplosionSmoke", frandom(-16, 16), frandom(-16, 16), frandom(0, 16), 0, 0, random(0, 4));
				}
			}
			TNT1 AA 1
			{
				if(LEG_Flames)
				{				
					for (int i = 0; i < 5; ++i)
						A_SpawnItemEx("LEG_Boom", frandom(-30, 30), frandom(-30, 30), frandom(-20, 20));
						
					for (int i = 0; i < 5; ++i)
						A_SpawnItemEx("LEG_BoomPlume", 0, 0, 16, 0, frandom(-12, 12), frandom(3, 12));
					
					for (int i = 0; i < 6; ++i)
						A_SpawnItemEx("LEG_BBBFlame", 0, frandom(-16, 16), frandom(0, 16), -1, frandom(-6, 6), random(1, 4));
				}
			}
			TNT1 AA 1
			{
				if(LEG_Flames)
				{				
					for (int i = 0; i < 8; ++i)
						A_SpawnItemEx("LEG_Boom", frandom(-50, 50), frandom(-50, 50), frandom(-40, 40));
						
					for (int i = 0; i < 7; ++i)
						A_SpawnItemEx("LEG_BoomPlume", 0, 0, 32, 0, frandom(-18, 18), frandom(3, 12));
					
					for (int i = 0; i < 12; ++i)
						A_SpawnItemEx("LEG_BBBFlame", 0, frandom(-24, 24), frandom(0, 24), -1, frandom(-6, 6), random(1, 4));
				}
			}
			TNT1 AA 1
			{
				if(LEG_Flames)
				{				
					for (int i = 0; i < 12; ++i)
						A_SpawnItemEx("LEG_Boom", frandom(-70, 70), frandom(-70, 70), frandom(-60, 60));
						
					for (int i = 0; i < 9; ++i)
						A_SpawnItemEx("LEG_BoomPlume", 0, 0, 48, 0, frandom(-12, 12), frandom(3, 12));
					
					for (int i = 0; i < 16; ++i)
						A_SpawnItemEx("LEG_BBBFlame", 0, frandom(-48, 48), frandom(0, 32), -1, frandom(-6, 6), random(1, 4));
				}
			}
			TNT1 AAAAAAAAAA 2 
			{
				if(LEG_Smoke)
				{
					for (int i = 0; i < random(1, 3); ++i)
						A_SpawnItemEx("LEG_FireballSmoke", frandom(-16, 16), frandom(-16, 16), frandom(0, 16), 0, 0, random(0, 2));
				}
			}
			Stop;
	}
}

class ChargingBruiserBall : Actor
{
  Default
  {
    Projectile;

    Scale 0.65;
    Alpha 0.75;

    Renderstyle "Translucent";

    +NOINTERACTION;
    +BRIGHT;
    +ROLLSPRITE;
  }

  States
  {
    Spawn:
      LFB] A 3 NoDelay
      {
        A_SetRoll(frandom(0, 360));
        bSpriteFlip = random(0, 1);
      }
      Stop;

    Death:
      TNT1 A 1;
      Stop;
  }
}