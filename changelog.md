# **---UPDATE 4.5 ---**
## July 7, 2024
## General
* ZScript version updated to 4.12.0
* Monster death explosion damage and radius increased. Any specific references to increases/decreases in either one of these following is in addition to this.
* Ice VFX are now effected by visual CVars
## Misc
* Base Projectile class created and most projectiles now inherit from it
* Explosion VFX tweaked
* Ice VFX tweaked
* Lightning VFX tweaked
* Bone dust VFX tweaked
* Fixed bones getting stuck in the air on elevators
* Various gibbs now inherit from Base Gibb
## Monsters
### - HUMANS -
* Grenades no longer bounce on TERRAIN defined liquids

### - IMPS -

__Dark Imp__
* Flurry attack amount increased

__Cyber-Imp__
* Explosion damage increased
* Explosion radius increased

__Harvester__
* Ball damage increased

### - LOST SOULS -

__Lost Soul__
* Bolt death VFX updated

__Damned Soul__
* Fixed being able to be killed while detonating
* Bolt death VFX updated

### - CACODEMONS -
* Base ball speed increased
* Attack windup added

__Cacodemon__
* Ball speed decreased
* Ball VFX updated
* Cacobeam VFX updated
* Cacobeam now ignores armor

__Cacoferno__
* Pre-explosion VFX updated
* Active VFX updated
* Ball speed increased
* Explosion damage increased
* Explosion radius increased
* Explosion VFX updated

__Cacoboreal__
* Ball speed increased
* Fixed Cacoram charing SFX playing forever if Cacoram fails

__Cacolich__
* Ball damage increased

__Broodmother__
* Attack spread decreased
* Attack now has vertical spread
* Attack animation adjusted
* Attack amount decreased
* Projectiles now have different spread based on type

### - MANCUBI -
* Gibb explosion damage increased
* Fixed inconsistent attack timings
* Aiming VFX added

__Cryo Mancubus__
* Projectile speed increased

__Dark Mancubus__
* Fixed projectiles making flying sounds after impact
* Projectile damage decreased

### - ARCHVILES -
__Boreal Archvile__
* Death VFX updated

__Voltaic Archvile__
* Attack speed decreased
* Teleport speed increased
* Teleport recovery increased
* Teleport VFX updated
* Teleport SFX updated
* Death VFX updated
* Is now completely invulnerable while teleporting
* Fixed retaining attack charge through a pain triggered teleport

### -BARONS OF HELL -
* Base Baron Ball speed increased
* Counter mechanics now effected by Extra Attacks option
* Counter mechanics overhauled
  * Instead of throwing lower delay counter attacks, build up a counter meter when hit. Meter resets on a successful attack. Depending on the state of the meter, will attempt normal attacks, Special attacks and eventually become immune to pain.  

__Baron of Hell__
* Incineration projectile speed increased
* Fixed sprite alignment issue causing them to vanish from certain angles while attacking

__Baron of Flames__
* Ball speed increased
* Active VFX updated
* Hellfire Breath VFX updated

__Baron Of Rime__
* Ball speed increased

__Baron of Darkness__
* Attack speed decreased
* Ball damage increased
* Ball speed decreased

__Cyber-Baron__
* Explosion radius increased
* Explosion damage increased
* Explosion VFX updated

__Bruiser Demon__
* Attack speed increased
* Fixed Pyroclasm preserving attack charge

### - ARACHNOTRONS -

__Arachnotron__
* 'Melee' attack speed decreased
* 'Melee' attack recovery increased

__Arachnodrone__
* Beam pre-detonation burning damage radius increased
* Beam detonation damage increased
* Beam detonation radius increased
* Can no longer rotate while firing
* No longer interrupts attack if LOS is broken

### - CYBERDEMONS -
* Fixed always spawning as the visual variant
* Explosion VFX updated
* Explosion SFX updated

### - SPIDER MASTERMINDS -
* Fixed waiting forever if LOS is broken while winding up
* Fixed warning alarm continuing to play after death
* Now wind down after firing
* No longer instantly stops shooting after loosing LOS
* Explosion VFX updated
* Explosion SFX updated
* Barrier can no longer be RIPPER'd


# -------------------------------------------------------------

# **--- UPDATE 4.0 ---**
## Feb 11, 2024
## General
- Brightmaps added for most monsters
- In game Codex removed. Instead there is a Codex document viewable on the git repo and in the zip file
- Readme added, explaining general concepts and mechanics
- Sprites that were Translations have been updated to be manually recolored
- Species Spawn CVar now fully functional
- Species Spawn CVar now works in reverse, to make more sense
- Boss Spawn CVar now fully functional
- Boss Spawn CVar is now independent of Species Spawn CVar
- Special Attack Cvar is now fully functional
- Extra Properties CVar is now fully functional
- Extra Properties CVar now effects more properties, like Human bullet damage and Undead resurrection 
- Status CVar added. Toggles the infliction of elemental based status effects
- Boss Warning message CVar added
- Gore CVar is now much more effective
- Status Effects are now inflicted much more reliably
- Burning now actually sets things on fire
- Burning damage increased
- Burning duration increased
- Burning damage interval increased
- Hell Burning duration reduced
- Hell Burning duration can now stack
- Magnetized duration can no longer be refreshed or stacked
- Magnetized now doubles Plasma damage taken
- More pain sound spam fixed
## Misc
- Code almost totally refactored, and all enemies inherit from a base class now. Results in much cleaner execution and easier to manage modding
- Fixed some monsters double dropping items
- Lots of file reorganization
- Map placed human and imp corpses are now randomized
- Gibbs physics updated
- Gibb landing sound added
- Green gibbs added
- Blue gibbs added

## Monsters
- New monster, Torment Elemental, added. Pain Elemental but Ice.
- Undead corpse mechanics refactored. Now less janky and stupid, and they can be squashed by doors, crushers, etc.
- Undead are now lit on fire by any Fire damage
- Cybers no longer wait for you if line of sight is broken before they start firing but after they start aiming
- Cybers now have a 100% pain chance to Electric attacks
- Cybers now have a unique extended pain state to Electric attacks
- Cybers now resist Physical
- Cybers SmallArms resistance decreased
- Monsters who turn slowly before attacking will now keep turning until they are fully facing their target before firing

### - HUMANS -
* Shotgun wielders now drop Shells in addition to a shotgun
* Bullet now comes out closer to gun's placement on sprite
* Now randomly left handed
* Size randomization for all humans added
* Size randomization now effect hitbox
* Now lose aim charge when repositioning
* Aim time increased overall
* SFX added when starting to aim
* LMG aiming time increased
* SMG aiming time decreased
* Grenade fuse time decreased
* Grenade physics updated

__Faculty__
* Shotgun pump speed decreased

__Soldiers__
* Wake up SFX updated
* Now much more likely to keep shooting after losing line of sight
* Now actually face you when throwing grenades

__Commandos__
* Wake up SFX updated
* Now have a chance to wait and keep aiming after losing line of sight, shooting as soon as line of sight is re-established
* AR now fire highly accurate burst fire at long ranged
* LMG are now much more likely to keep shotting after losing line of sight or over a certain range

__Zombie__
* Size is now randomized
* Now wander
* Melee range increased

__Unwilling__
* Melee speed reduced
* Fixed hitbox being much smaller than intended

__Zombie Horde__
* Minimum zombie count increased to 20 from 5
* Maximum zombie count increased to 150 from 100

__Cyborg Scout__
- Pain threshold decreased
* Completely remade

__Cyborg Commando__
* Fixed aim "charge" never being cleared
* Pain threshold decreased
* Bullet damage decreased
* PIKE damage increased
* PIKE now properly does Piercing damage instead of Peircing

### - IMPS -
__Frost Imp__
* Melee threshold reduced

__Cyber Imp__
* Fixed getting stuck in melee when target dies
* Fixed missing sounds

__Harvester__
* Attack speed decreased
* Soul Harvest VFX updated
* Soul Harvest damage increased
* Soul Harvest speed decreased

__Imp Mage__
* Fire cast speed decreased
* Fire big fireball damage decreased
* Bolt cast speed increased
* Ice cast speed decreased
* Ice damage decreased

### - PINKIES -
* Fake death timer is now randomized between 4 and 10 seconds
* Pain time increased

__Spectre__
* Fixed missing sprites
* Fades in more when pained
* Can no longer be targeted by homing attacks
* Can no longer be targeted by other monsters

__Dark Pinky__
* Now has Fake Death mechanics
* Melee damage reduced

__Cyber Pinky__
* Melee range increased

### - LOST SOULS -
* Ram attacks now deal impact damage
* Ram speed decreased
* Attack speed decreased
* Attack damage decreased
* Damned Souls HP decreased to 5 from 15
* Damned Souls now have a higher pitched scream
* Damned Souls detonation time increased
* Damned Souls detonation can no longer be interrupted
* Damned Souls now face target when detonating
* Damned Souls detonation SFX added
* Damned Souls explosion damage increased
* Damned Souls explosion radius increased
* Damned Souls no longer wander

__Lost Soul, Bolt__
* Fixed attack doing Fire damage

__Lost Soul, Ice__
* Fixed attack doing Fire damage
* Sprites updated

### - REVENANTS -
* Melee threshold decreased
* Fixed resurrecting fully equipped

__Revenant__
* Pre-fire frame added
* Rocket speed increased
* Attack speed increased
* Attack recovery decreased

__Heavy Revenant__
* Now loses Leviathan charge when pained
* Leviathan damage increased

__Para-Revenant__
* No longer explodes
* Sprites are now complete
* Shots can now be heard from further away
* Jet pack can be heard from further away
* Now faces target when shooting
* Now falls when it dies while flying
* No longer immune to pain while flying
* Now falls when Pained while flying
* Pain state extended while flying
* Flying sprite adjusted to more closely match hitbox
* Grounded attack recovery increased
* Flying attack recovery increased
* Rocket damage increased
* Rocket now does Fire damage

### - CACODEMONS -
* Much more reliably turns aggressive when enraged
* Now becomes enraged after taking any damage
* Now dash back after biting
* Now lose all speed when shooting

__Cacodemon__
* Cacobeam now does set damage of 150 instead of a wide range
* Cacobeam speed increased

__Cacoferno__
* Fixed ball not doing Fire damage
* Spit speed decreased
* Special attack, Hellfire Breath, added
* Active VFX reduced
* Gibb VFX tweaked

__Cacoboreal__
* Special attack, Cacoram, added
* Spit speed decreased
* Fixed ball not doing Ice damage

__Cacolich__
* Special attack, Death Stare, added
* Spit speed decreased

__Broodmother__
* No longer needs line of sight to become enraged at Cacobaby death
* Delay added after teleporting before she can attack

### - HELL KNIGHTS -
__Hell Knight__
* Melee recovery increased
* Attack speed decreased
* Attack recovery increased
* Pounce charge time increased

__Cyber Knight__
* Pain Threshold decreased
* Now has unique SFX for plasma and grenades
* Now fires plasma in bursts of 3
* Now always switches to Plasmathrower if target is in melee range while shooting
* No longer throws grenades in melee range
* Melee recovery time increased

__Labolas__
* Enraged melee speed reduced
* Fixed always being enraged
* Speed decreased
* Pounce delay increased
* Ground Slam delay increased
* Ground Slam landing lag increased
* Now roars when preparing to Ground Slam

### - PAIN ELEMENTALS -
* Death explosion now causes Status Effects
* Special Attack threshold increased
* Soul reserves are now emptied after using Swarm

### - MANCUBUSSYS -
* Much more responsive overall
* Attack pattern is now random

__Mancubus__
* Fixed getting stuck while using flamethrower

__Dark Mancubus__
* Attack speed increased

__Cryo Mancubus__
- Fixed getting stuck while using freezethrower
- Fixed missing firing SFX
- Attack speed decreased
- Freezethrower range increased
- Freezethrower now inflicts Chilled

### - ARACHNOTRONS -
* Barrier is now refreshed when revived
* Barriers will now remove themselves if owner is removed
* Barriers are now disrupted by melee attacks
* Pain chance for melee damage is now 100%

__Arachnotron__
* Now much more likely to keep shooting after losing line of sight
* Warning beeps added to self-destruction
* Pain time increased
* Heavy Suppression removed
* Melee attack added
* No longer able to shoot through each other
* Can now damage each other
* Will now check lines of fire and reposition if something is in the way
* Will now check that no enemies are in line of sight before renewing barrier. Has a limited range
* Now actually aim up and down

__Arachnodrone__
* HP reduced to 600 from 900
* Now spawns with a barrier
* Explodes on destruction
* Corpse begins to arc, then light on fire, as it takes damage

### - BARONS OF HELL -
* No longer constantly throws balls when in line of sight
* Counter attack mechanic is now universal

__Baron of Flames__
* Now has a Melee Threshold
* Now throws in bursts of two
* Melee damage increased
* Melee speed increased
* Melee recovery decreased
* Melee attacks now do both Melee and Hellfire damage

__Baron of Darkness__
* Attack speed increased
* Baron Bolt SFX can now be heard globally

__Baron of Rime__
* Fixed attacks not doing Ice damage
* Now leaps towards target when using Baron Blizzard
* Baron Blizzard damage is now random
* Attack speed reduced

__Cyber Baron__
* Pain threshold decreased
* Turn speed is now the same across all attacks
* Projectile size decreased
* Melee damage increased
* Azazel rate of fire increased
* Azazel is now fired in bursts of three
* Belial fire delay decreased
* Belial activation range decreased

__Baron Mage__
* Will now use Special Attacks in its first phase
* Spread attack now throws Ice

__Bruiser Demon__
* Completely remade

### - ARCHVILES -
* Fixed various rune VFX never actually being removed
* Fixed retaining Focus charge through pain

__Archvile__
* Magic Missile removed
* Fire wave charge time decreased

__Voltaic Archvile__
* Attacks are now easier to tell apart
* Focus speed increased
* Focus direct damage increased
* Focus explosive damage removed
* Lightning Bolt cast speed increased
* Teleport is now less likely to go nowhere
* Teleport start up time increased
* Teleport recovery time increased
* Summon animation added
* Summon speed decreased

__Boreal Archvile__
* Now fires iceballs in bursts of three
* Focus speed decreased
* Focus minimum damage decreased
* Iceball cast speed decreased
* Ice ball speed decreased

__Wizard__
* Magic Missile VFX added
* Ice Storm is now centered on the Wizard instead of throwing it
* Death is delayed while Ice Storm is in effect
* Ice Storm can no longer be interrupted 
* Ice Storm casting time increased

### - CYBERDEMONS -
* Visual variant added
* THICCNESS increased
* Resistances updated to Proto class demon resistances
* Fixed firing SFX not playing properly
* Rockets now come out closer to its cannon
* Turns for longer before firing
* Turning speed increased
* Now stops firing to turn faster at wide angles
* Can now properly damage and infight with each other
* Rockets now fly towards where its aiming, instead of directly at target
* Rocket acceleration increased
* Rate of fire increased
* No longer instantly transitions from Main Cannon to Suppressive Rockets
* Less likely to use Suppressive Rockets
* Main Cannon less likely to keep firing when line of sight is broken
* Suppressive Rockets more likely to keep firing when line of sight is broken
* Suppressive Rockets now have slight homing

### - SPIDER MASTERMINDS -
* Now has a 100% Pain Chance to Melee damage
* Wind up animation added
* Missing resistances added
* Micro Missile firing SFX added
* Micro Missile aiming time increased
* Micro Missile alarm SFX updated
* Will now give up on Micro Missiles if a target is not found
* Turn speed reduced

# **--- UPDATE 3.5 ---**
## -- Oct 3 2023 -- 

## General
- Bosses, Greater Demons and Undead are now immune to fear
- Bosses can no longer spawn on the first map (doesn't always work)
- Fixed some bosses ignoring the Boss Spawn CVar
- Fixed several monsters spamming their pain sound while stunned

## Misc
- Tracer sizes reduced 
- Grave VFX updated 
- Grave SFX added 
- Flame VFX updated
- Ice VFX updated
- Explosion VFX updated
- Various death animations updated and tweaked
- Various body thumps added 
- Codex formatting udpated
- Codex graphics updated 
- Codex typos fixed 
- Lots of under the hood code slimming and refactoring

## Monsters
- New "Boss", Zombie Hoard, added. Replaces Zombieman
- Agony Elemental removed
- Faculty, LMG removed
- Undead corpses no longer have damage thrust

### - HUMANS -
- Human sounds updated
- Human pain time increased 
- Human pain time is now random 
- Human visual size is now random. Hitbox is unchanged
- Pistol accuracy increased 
- Shotgun accuracy increased 

__Faculty__
  - Accuracy increased 

__Soldier__
  - If killed or pained after pulling the pin on a grenade, but before throwing it, will drop a live grenade at their feet
  - Accuracy increased
  - Sprites updated
  - Alt deaths added
  - Skin tone is now randomized
  - Armor durabilty increased
  - Armor defense increased 
  - Grenade throw delay decreased
  - AR aim time increased 
  - SMG aim time increased 
  - LMG aim time reduced
  
__Commando__
  - Armor durabilty increased   
  - Armor defense increased
  - Armor LongArms resistance reduced 
  - Armor Plasma resistance reduced  
  - AR no longer does weird...strafing...snap shot...thing
  
__Zombie__
  - Fixed one of the visual variants attacking faster than the others
  
__Unwilling__
  - Blood color changed to green
  
__Cyborg Scout__
  - Now spawns over Shotgunner spawns, instead of any Soldier spawn
  
__Cyborg Commando__
  - Now spawns over Chaingunner spawns, instead of any Commando spawn
  
### - IMPS -
- Melee speed reduced

__Imp__
  - Big fireball damage increased 
  
__Dark Imp__
  - Tag updated 
  - Attack speed increased 
  - Flurry charge time reduced 
  
__Ice Imp__
  - Now known as a Frost Imp
  - Tag updated
  - Fixed projectiles not doing Ice damage
  - Attack speed reduced 
  - Special attack, Bouncing Ice, added
  
__Cyber Imp__
  - Melee timing tweaked 
  - Melee damage increased 
  - Pain time increased 

__Harvester__
  - Gibbs added 
  - Missing death sound added
  - Grave ball damage increased 
  - Grave ball speed increased 
  - Soul Steal damage increased
  
__Imp Mage__
  - Spawn chance reduced
  - Overall attack speed reduced 
  - Now selects all attacks equally 
  - Homing fireball speed reduced 
  - Death sequence added 
  
### - PINKIES -

__Pinky__
  - Third visual variant added 
  - Spawn chance increased
  - Tag updated 
  - Melee recovery increased 
  - Properly dies after faking death  
  
__Spectre__
  - Melee recovery increased
  - Properly dies after faking death

__Dark Pinky__
  - Tag updated
  - Melee speed reduced 
  - Leap charge time increased 
  
__Cyber-Pinky__
  - Spawn chance reduced
  - Melee recovery increased
  
### - LOST SOULS -
- Aim time increased 
- Melee recovery time increased 

__Lost Soul__
  - +QUICKTORETAILATE flag removed

### - REVENANTS -
- Melee speed reduced 
- Melee recovery time increased 
- Can now damage and infight each other 

__Revenant__
  - Fixed refire timing being off

__Para-Revenant__
  - No longer wears armor
  - No longer burst fires
  - Increased fire delay when flying
  - Now fires a burst of 2 rockets while flying, instead of both at once
  - Delay added between targeting and firing
  - Rocket speed reduced 
  
__Heavy Revenant__
  - Fixed wrong sprites being used while firing
  - Firing sounds updated 
  - Grave shot firing sprite added 
  - Grave shot VFX added 
  - Grave shot firing speed reduced
  - Leviathan can now be fired more frequently
  - Leviathan charge time increased 
  - Leviathan speed increased 
  - Pain time increased 
  - Pain chance increased 
  
### - CACODEMONS -

__Cacodemon__
  - Acive VFX reduced 
  
__Cacoflame__
  - Explosion delay increased 
  - Now properly faces target when meleeing
 
__Cacolich__
  - Gibbs added 
  - Melee is now consistant with other Cacodemons 
  
__Broodmother__
  - Updated Baby Caco sprites
  - Added Baby Caco SFX 
  - Baby Caco health increased 
  - Fixed projectiles passing through Baby Cacos
  - Baby Caco +BOSS flag removed 
  - Baby Caco now has +NOTARGET flag
  - Now spawns a random amount of Baby Cacos
  - Enraged SFX added 
  - Teleport SFX added 
  - Hellfire Beam sound cue added
  - Hellfire Beam SFX added
  - Hellbolt sound cue added 
  - Hellbolt charge time increased 
  - Fixed missing pain sound 
  - Fixed missing death sound 
  - Fixed gibbing Baby Cacos preventing enragement
  - Fixed spawning Baby Cacos twice
  - Fixed spawning Baby Cacos when despawned 
  
### - HELL KNIGHTS -

__Hell Knight__
  - No longer builds up rage while stunned 
  - Pain time increased
  - Melee recovery time increased 
  - No longer double hit while enraged 
  - Enraged melee speed reduced
  - Now immune to damage thrust while enraged
  
__Cyber Knight__
  - Melee recovery time increased 
  - Plasma thrower recovery time increased  
  - Pain time increased 
  - Plasma VFX tweaked 
  
__Labolas__
  - Melee speed reduced

### - PAIN ELEMENTALS -
- Attack recovery time increased 
- Death explosion delay increased 
- Now tracks the player during Swarm
- Swarm rate of fire increased 

### - ARACHNOTRONS -

__Arachnotron__
  - Barrier SmallArms resistance reduced 
  - Barrier destruction animation added 
  - Barrier tag added 
  
__Arachnodrone__
  - Translation tweaked 
  - Attacks are properly tracked
  - Aggression increased 
  - Attack cooldown reduced 
  - Attack now begins closer to them 
  - Attack explosion delay reduced 
  - Attack now does DoT while ground is burning
  - Attack explosion damage increased 
  - Attack explosion radius increased 
  - Corpse no longer explodes instantly on destruction
  
### - BARONS OF HELL -
- Updated casting sounds on elemental Barons
- Melee speed reduced 
- Melee recovery increased 
- Pain time increased 
- Counter throw speed reduced

__Baron of Hell__
  - Spread throw is now done with both hands and spread both ways
  - Visual variant sprite tweaked 
  - Visual variant brightmaps added
  
__Fireborne Baron of Hell__
  - Now known as Baron of Flames 
  - Fixed abnormal melee timing
  
__Iceborne Baron Of Hell__
  - Now known as Baron of Rime 

__Baron Mage__
  - Completely reworked into a mini-boss
  - Lowered spawn chance
  
__Bruiser Demon__
  - Updated SFX
 
### ARCHVILES
- Revive runes are now proper pentagrams 
- Armor durability reduced 
- Defensive ability chance increased 

__Boreal Archvile__
  - Defensive ability, Ice Clone, added 

### SPIDER MASTERMINDS 
- Barrier destruction animation added 
- Turn speed while aiming increased 
- Barrier tag added