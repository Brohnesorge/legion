# --- CODEX OF DEMONS ---
This document features detailed information on all the enemies present in LEGION, including attacks, stats and lore. Exact damage values will not be shared, and the lore presented will be a supplementary to the lore presented in the forum threads. Boss HP will not be revealed, but is never less than 500. The headers will be the vanilla monster spawn. Format for information will be as follows
### Name
* HP
* Type: Monster type. See readme.
* Special: Special Attack description. Alternate attacks will be listed here and labeled as such.
* Extra: Extra properties
* Resistances, other than Type resistances
* Unique: Any unique traits or rules to that monster

## --- Zombieman, Shotgunner, Chaingunner ---
Humans, living and dead, possessed by the countless evil souls that came pouring forth from Hell's maw, or traitors tempted by promises of power and desires fulfilled.

Instead of having set spawns, humans are split into 3 'species': Faculty, Soldier and Commando. These are mixed across the three spawns, with random weapons. Weapons have limited magazine capacities and must be reloaded, but have unlimited reserves. Zombies and Unwilling can spawn over Zombiemen only, and each spawn slot has its own boss.

### Faculty
* HP: 20
* Type: Human
* Extra: Uses a random weapon selected from Pistol, Shotgun, SMG, Assault Rifle
* Unique: Will check fire lines, and moves to avoid shooting allies

Faculty members, office works and other civilian contractors from around the facility. They can barely use those guns they are holding and posses no special abilities.

### Soldier
* HP: 30
* Type: Human 
* Special: Grenade. Throws a fragmentation grenade. Carries a limited number between 0 and 2.
* Extra: Uses a random weapon selected from Shotgun, SMG, Assault Rifle, LMG
* Extra: Lays down suppressive fire
* Extra: Wears soft armor
* Unique: Will check fire lines, and moves to avoid shooting allies

Soldiers from various governments' military or corporate security forces. May also be insurgents. Well trained and takes time to actually aim those guns. 

### Commando
* HP: 50
* Type: Human
* Special: Grenade. Throws a fragmentation grenade. Carries a limited number between 1 and 4
* Extra: Uses a random weapon selected from Shotgun, SMG, Assault Rifle, LMG
* Extra: Waits and continues to aim for a short time after target breaks line of sight
* Extra: Wears hard armor
* Extra: Gains additional attacks based on weapon used
  * Shotgun: Uses a double barrel instead, dealing greater damage but has to reload after every shot
  * SMG: N/A
  * Assault Rifle: Switches to rapid and accurate 3 round bursts at long ranges
  * LMG: Fires continuously at long ranges, emptying magazine regardless of line of sight
* Unique: Will check fire lines, and moves to avoid shooting allies

Elite commandos sent on black ops missions. Extremely skilled and dangerous

### Zombie
* HP: 40
* Type: Undead
* Unique: Spawns in groups and can move through each other. 
* Unique: Wanders aimlessly when not alerted. 
* Unique: Does not infight its own species.
* Unique: Low chance to drop ammo or healing supplies.

Possessed and decayed corpses. Shambles about looking for something to maul. Lacks any form of ranged attack, moves slowly and is easily ignored.

### Unwilling 
* HP: 25
* Type: Demon, Lesser
* Unique: Spawns in groups and can move through each other. 
* Unique: Does not infight its own species.

Humans corrupted by Hell's influence on reality. Can also be formed in Hell by wandering souls gaining form. Quick, but lacks any kind of ranged attack. Almost always found in packs.

### Zombie Horde (Zombieman Boss)
* Unique: A huge group of Zombies

A mass walking grave. Wether the invasion has taken a toll on the human populace, giving wandering spirits plenty of bodies to possess or a mass conversion of living to undead from the energies of Hell portals being torn open, humongous groups of the walking dead are a dreadfully common sight. 

### Cyborg Scout (Shotgunner Boss)
* HP: ???
* Type: Human/Cyber
* Special: Underbarrel Grenade Launcher: Fires a 40mm grenade. Carries a limited amount between 4 and 10
* Extra: Uses an assault rifle
* Extra: Uses a powerful sniper rifle at long ranges. Carries 10 bullets and never reloads
* Unique: Will check fire lines, and moves to avoid shooting allies
* Unique: Fires in bursts at longer ranges
* Unique: Drops several live grenades on death

Cybernized soldiers specializing in recon. Despite the advanced technology on display, still easily possessed by demonic spirits. Something to do with the machine programing mixed with human thought, perhaps?

### Cyborg Commando (Chaingunner Boss)
* HP: ???
* Type: Human/Cyber
* Special: PIKE Launcher. Fires a rocket grenade
* Special: Backup. Calls for backup, summoning 4 Soldiers.
* Extra: Uses a grafted GPMG
* Extra: Lays down suppressive fire
* Extra: Wears hard armor
* Unique: Has unlimited ammo and doesn't reload. 
* Unique: Will check fire lines, and moves to avoid shooting allies

The best of the best, heavily cybernized soldiers grafted with a general purpose machine gun and a grenade launcher. Thankfully rare. Few volunteer for the disfiguring operation, even after possession.

## --- Imp ---
The most common type of demon, and the lowest classed intelligent demon in Hell's hierarchy. Despite this, they are still much stronger and tougher and humans, able to survive buckshot blasts. Capable of great magical and physical feats, and subject to several harmless physical mutations.

Their Sin is Sloth, for they rarely realize their full potential

### Imp
* HP: 60
* Type: Demon, Lesser
* Special: Big Fireball. Throws a bigger, faster, stronger fireball.
* Extra: Capable of a lunging melee attack

The most common type of Imp, also known as a Brown Imp. Agile and able to control fire. Their brown coloring is a thin layer of fur.

### Dark Imp
* HP: 60
* Type: Demon, Lesser
* Special: Flurry. Throws several lightning balls in quick succession
* Extra: Capable of a lunging melee attack

Imps from the subterranean 'Dark' levels of Hell. Manipulates electricity and lacks the fur of their surface brethren. Behaves almost identically to common Imps.

### Frost Imp 
* HP: 60
* Type: Demon, Lesser
* Special: Bounding Ice. Throws a big ice ball that bounces twice before shattering.
* Extra: Capable of a lunging melee attack

Imps from the colder circles of Hell. Controls ice and adapted to blend into their snowy surrounding. Slightly less inclined to jump into the fray, but still loves the feel of fresh blood on their claws.

### Cyber Imp
* HP: 120
* Type: Demon, Lesser/Cyber

Imps of various types that have taken The Spiders' offer of power, or ones that were disfigures and 'repaired'. Their new cybernetics interfere with their ability to channel mana. Nowhere near as agile, but the new weaponry more than makes up for it.

### Harvester
* HP: 90
* Type: Undead
* Special: Soul Harvest. Throws a powerful Unholy projectile that heals the Harvester if it hits.

Imps that have mastered Necromancy to achieve Lichdom. Effectively immortal, they harness the power of Grave to drain the life energy from living creatures and expend this energy to resurrect in their lairs, even when destroyed. While Necromancy is a common practice in Hell, using it on oneself to become a Lich such as this is quite rare, as the process is extremely difficult and failure results in death.

### Imp Mage (Boss)
* HP: ???
* Type: Demon, Greater
* Special: Baron Magic. Throws 3 Magic fireballs in quick succession
* Extra: Uses powerful Fire, Ice and Lightning attacks capable of inflicting Status

Powerful Imp magi who have tapped into their hidden potential. Able to efficiently channel Mana to unleash a flurry of elemental spells. Highly respected for their wide arsenal of destructive spells, even among Barons of Hell and, to a lesser extent, Archviles. 

## --- Pinky ---
Extra: Has a low chance to resist death, getting back up after a random delay with full HP but rapidly bleeding out.
### Pinky
* HP: 150
* Type: Demon, Lesser
* Extra: Regenerates health slowly

### Dark Pinky
* HP: 60
* Type: Demon, Lesser
* Special: Lunge. Can lunge short distances to get into melee range

### Cyber Pinky
* HP: 500
* Type: Demon, Lesser/Cyber

## --- Lost Soul ---
Extra: Attacks do Elemental damage.
### Lost Soul
* HP: 15
* Extra: Can be attuned to Fire, Electric or Ice.

### Damned Soul
* HP: 5
* Extra: Can be attuned to Fire, Electric or Ice.
* Extra: Can self-detonate.
* Unique: Explodes on death for Elemental damage.

## --- Revenant ---
Extra: Armor breaks on death, disabling weapons on revival. 
### Revenant
* HP: 300
* Type: Undead
* Extra: Wears fragile armor
* Unique: Randomly fires homing or non-homing rockets. Homing rockets have a limited amount of time they home before becoming normal, non-homing rockets.

### Heavy Revenant
* HP: 300
* Type: Undead
* Special: Leviathan Missile Launcher. Fires an extremely powerful missile with a large explosive radius.
* Resistances: Takes full damage from **Hellfire** and **Piercing**. Resists **Fire** and **LongArms**. Greatly resists **Ice**, **Electric**, **Plasma**, **SmallArms**, **Magic** and **Melee**. Takes half damage from all other sources.
* Unique: Explodes on death.
* Unique: Will always use Special Attack when its ready.

### Para-Revenant
* HP: 300
* Type: Undead
* Unique: Can activate jetpack and start flying. Pain causes it to fall.

## --- Cacodemon ---
Extra: High chance to be docile, but will become hostile when attacked. Wanders aimlessly while idle. Special Attacks are very slow but disproportionately powerful.
### Cacodemon
* HP: 400
* Type: Demon, Middle
* Special: Cacobeam. Fires an extremely powerful, extremely fast Electric projectile.
* Resistances: Greatly resists **Electric** and **Plasma**.
* Extra: Can rapidly dodge attacks

### Cacoboreal
* HP: 400
* Type: Demon, Middle
* Special: Cacoram. Charges forward, attempting to ram for extremely high Ice damage
* Resistances: Greatly resists **Ice**. Weak to **Fire**.
* Extra: Can rapidly dodge attacks

### Cacoferno
* HP: 400
* Type: Demon, Middle
* Special: Hellfire Breath. Spews a constant stream of Hellfire that deals heavy Hellfire damage with unlimited range
* Resistances: Greatly resists **Fire** and **Hellfire**. Weak to **Ice**. Very weak to **PLWater**.
* Extra: Explodes on death

### Cacolich
* HP: 600
* Type: Undead
* Special: Cacostare. Death gaze deals heavy Unholy damage and heals the Cacolich

### Broodmother (Boss)
* HP: ???
* Type: Demon, Greater
* Special: Hellfire Beam. Fires a constant instant stream of Hellfire from her eye.
* Special: Hellbolt Storm. Lightning strikes in an huge area around.
* Unique: Always spawns docile.
* Unique: Spawns with a large amount of docile, harmless Cacobabies. If a Cacobaby dies, the Broodmother becomes hostile.
* Unique: Teleports in response to pain.

## --- Hell Knight ---
Extra: Pain builds up rage. At max rage, goes berserk, increasing speed, resisting damage and becoming immune to pain, but becoming unable to use ranged attacks.
### Hell Knight
* HP: 500
* Type: Demon, Middle

### Cyber Knight
* HP: 1000
* Type: Demon, Middle/Cyber
* Special: Grenade launcher. Fires a grenade that explodes on impact with target, or after a short duration.
* Extra: Can use a Plasmathrower in melee range.
* Unique: Does not go berserk.

### Labolas (Boss)
* HP: ???
* Type: Demon, Greater
* Special (Alternate): Lunge. Lunges at target.
* Special: Leaping Ground Slam. Roars, then jumps at target before slamming the ground, dealing large damage in an area.
* Unique: Lacks a ranged attack
* Unique: Builds up rage passively

## --- Pain Elemental ---
Horrible balls of hate whose very innards burn with the fires of Hell. No way related to the less malevolent Cacodemon, despite visual similarities. Named for the great pleasure they take in causing any and all other creatures around them agony in any form. The souls they consume burn within them in constant pain. Greatly hated, even by other denizens of Hell

Their Sin is Envy, for they hate all around them and desire all they do not own.

Extra: Explodes on death. Death explosion is considered a high level Elemental attack.
### Pain Elemental
* HP: 400
* Type: Demon, Middle
* Resistances: Resists **Ice**. Greatly resists **Fire**. Weak to **PLWater**.
* Special: Swarm. Fires a rapid stream of Lost Souls. Supply of Lost Souls is emptied afterwards.
* Unique: Fire element. Has a limited supply of Lost Souls that regenerate over time.
* Unique: Spits small fireballs randomly and when out of Lost Souls.

### Torment Elemental
* HP: 400
* Type: Demon, Middle
* Resistances: Immune to **Ice**.
* Special: Swarm. Fires a rapid stream of Lost Souls. Supply of Lost Souls is emptied afterwards.
* Unique: Ice element. Has a limited supply of ice Lost Souls that regenerate over time.
* Unique: Spits small ice balls randomly and when out of Lost Souls.

## --- Mancubus ---
Large and corpulent demons. They lumber about, killing and eating anything they can get their claws on. One of the few demons that takes no issue in using weapons, the Astaroth Flame Cannons strapped to their arms are hooked directly into their bodies, weaponizing the gas they naturally produce.

Their Sin is Gluttony, for they consume all in their maws and their flames.

Extra: Fire damage and gibbing causes an explosion on death.
### Mancubus
* HP: 600
* Type: Demon, Middle
* Extra: Uses a flamethrower in melee

### Dark Mancubus
* HP: 600
* Type: Demon, Middle

### Cyro Mancubus
* HP: 600
* Type: Demon, Middle
* Extra: Uses a freezethrower in melee

## --- Baron of Hell ---
Towering behemoths of demons, also known as Hell Nobles. The former ruling class of Hell, these demons are physically powerful, highly intelligent and capable of casting powerful destruction magic. Many variants of this demon exist and all answer only to the Masterminds themselves. Their skin is capable of deflecting small arms fire, and their bones are nearly unbreakable.

Their Sin is Greed, for they desire the power of the dark gods they serve.

Extra: Taking damage builds up counter rage. Depending on the amount of counter rage, throws faster coutner attacks, counter special attacks and becomes immune to pain at max counter rage. Successfully using any attack resets counter rage. Resists SmallArms. Possesses several alternate and Special attacks.
### Baron of Hell
* HP: 1000
* Type: Demon, Greater
* Special: Spray (Alternate). Throws a wide spray of Baron Balls.
* Special: Incineration. Throws an explosive Magic fireball.

### Baron of Flames
* HP: 1000
* Type: Demon, Greater
* Special: Spray (Alternate). Throws a wide spray of fireballs.
* Unique: Lacks a Special Attack, but is much more aggressive.
* Unique: Melee attacks deal additional Hellfire damage.

### Baron of Darkness
* HP: 1000
* Type: Demon, Greater
* Special: Fulmination. Throws a hit-scan lightning bolt.

### Baron of Rime
* HP: 1000
* Type: Demon, Greater
* Special: Spray (Alternate). Throws a wide spray of ice balls
* Special: Whiteout. Ground slams and summons an ice storm in place.

### Cyber Baron
* HP: 2000
* Type: Demon, Greater/Cyber
* Special: Azazel (Alternate). Fires a burst of explosive autocannon shots.
* Special: Belial (Alternate). Fires a short range flak cannon.
* Special: Malphas. Fires a burst of homing rockets.

### Baron Mage
* HP: 1500
* Type: Demon, Greater
* Special: Spray (Alternate). Throws a wide spray of various elements of Baron Ball.
* Special: Incineration. Throws an explosive Magic fireball.
* Special: Fulmination. Throws a hit-scan lightning bolt.
* Special: Whiteout. Throws an ice storm.
* Unique: Special Attacks do not refresh its Special Timer when low on health.
* Unique: Throws all elements of Baron Ball.

### Bruiser Demon (Boss)
* HP: ???
* Type: Demon, Greater
* Special: Pyroclasm. Throws two explosive Hellfire balls in quick succession.
* Special: Firestorm. Ignites, then detonates the surrounding area. 
* Extra: Regenerates health. 
* Resistances: Immune to **Fire** and **Ice**. Strongly resists **Hellfire**. Very weak to **PLWater**.
* Unique: Passively ignites the surrounding area while walking and attacking.

## --- Archvile ---
The most demonic of demons, Archviles serve as generals of Hell's armies and guide the worship of its dark Icons. They command the respect of nearly all other demons, even the stubborn Nobles. Powerful magicians with the inherent abilities to summon or even revive other demons, and an unnatural level of durability. One of the only demons capable and willing to speak human language.

Their Sin is Pride, the greatest of Sins, for they known the power they wield.

Extra: Has force armor that greatly resists damage. Armor can be broken and will not refresh.
### Archvile
* HP: 700
* Type: Demon, Greater
* Special (Alternate): Fire Wave. Quick, ground based Fire projectile.
* Special: Ghosting. In response to pain, turns mostly invisible and flees at very high speed before stopping and becoming visible again. Can not attack while Ghosting.

The most common type of Archvile, also known as Promenence Achviles. Fast, durable and capable of burning anything to ashes. Worse yet is their mastery of necromancy, allowing them to reanimate the dead. Capable of casting any sort of magic, but focuses on fire. Able to cast multiple fire spells, and has access to some defensive magic.

### Boreal Archvile
* HP: 700
* Type: Demon, Greater
* Special (Alternate): Ice Ball. Fires three ice balls in quick succession.
* Special: Ice Clone. In response to pain, can shunt backwards, leaving behind a duriable ice decoy.

Archviles from the coldest circle of Hell. They encase enemies in magical ice and throw balls of ice. Their mastery of necormancy is unmatched, allowing them to reanimate the dead. Capable of casting any sort of magic, but focuses on ice. Able to cast multiple ice spells, and has access to some defensive magic.

### Voltaic Archvile
* HP: 700
* Type: Demon, Greater
* Special (Alternate): Lightning Bolt. Hitscan lightning attack.
* Extra: Can teleport while moving and in response to pain
* Unique: Can summon Lesser Demons. Does not revive fallen allies.

Archviles from the dark subterranean caverns of Hell. Also known as Dark Archviles, their skill with electricity magic and great intellect has lead to them inventing many devices used underground. Capable of casting any sort of magic, but focuses on lightning. Able to cast multiple lightning spells, and has access to some defensive magic. They've chosen instead to focus on conjuration magic, allowing them to summon groups of Lesser Demons to aid them.

### Wizard
* HP: 700
* Type: Demon, Greater
* Special (Alternate): Hellfireball. Fires an explosive fireball that deals Hellfire damage.
* Special (Alternate): Lightning Bolt. Hitscan Lightning attack.
* Special (Alternate): Snowstorm. Deals constant Ice damage in a large area around the Wizard. Wizard cannot die nor move during the duration. Dies afterwards if HP is 0.
* Special: Contingency Spells. Responds to pain at low health by warping away, leaving a trail of decoys before putting up a barrier. Barrier resists Elemental damage and Magic and must be destroyed before the Wizard can be harmed again.
* Unique: Can revive fallen allies and summon Lesser Demons.

Humans often sell their souls to demons for vague promises of power. Rarely, on of these humans is able to successfully bargain and gain use of powerful demon magics. When these witches die, should their souls be dark and tainted enough, they live again as one of these depraved abomanations. Powerful, quick and able to use a wide variety of magic, they will attempt to warp away and bolster their defenses if the situation turns against them.

## --- Arachnotron ---
Enforcers of the Spider Masterminds' will, these creatures patrol around on metal bodies made of an unknown alloy sporting powerful plasma repeaters. Between this chasses and their natural psionic barriers, Arachnotrons are much more durable than their appearence suggests.

Their Sin is none for they are not demons at all. These beings are the male form of the species while Spider Masterminds are the females.

### Arachnotron
* HP: 200
* Type: Brain
* Unique: Will check fire lines and move to avoid hitting allies.

### Arachnodrone
* HP: 600
* Type: Brain/Cyber/Undead
* Unique: Cannot refresh barrier manually. Barrier refreshes when revived.

## --- Spider Mastermind ---
Also known (incorrectly) as Spiderdemons, they stand atop the pillar of Hell, answering to no one as they direct the forces of Hell against Earth. Old records of Hell fail to mention these beings, appearing suddenly a mere three thousand years ago, bringing upheaval and cybernetics with them. Why they stand unopposed by the Demon Lords or the Icons is a mystery known only to them.

Extradimensional conquerors, they lay waste to all opposition that fails to fall under their rule.

Extra: Chassis grants resistance to Physical
### Spider Mastermind
* HP: 3000
* Type: Brain
* Special (Alternate): Micro Missiles. Fires a rapid stream of homing rockets. Aiming system can be heard.

## --- Protodemon ---
Gargantuan demons of immense power. Said to be the precursors to the venerable Demon Lords and the children of the Icons of Sin. Long thought to have been driven to extinction by the Demon Lords. Most commonly appears as the Cyberdemon, a perversion of their great forms as little more than an animated corpse filled to the brim with cybernetics.

Still, some Protodemons may yet still live. If they do, they must be exceedingly rare, excessively powerful and ancient beyond measure.

Extra: Takes reduced splash damage
### Cyberdemon
* HP: 5000
* Type: Demon, Proto/Cyber
* Special: Suppressive rockets. Full auto onslaught of drunk, semi-homing rockets.
* Unique: Ignores Special Timer and can use its Special Attack at any time