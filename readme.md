# --- General Mechanics ---
* Enemies can infight within the same species
* Enemies have side-graded species variants
* More enemies can be gibbed, and cannot be revived if gibbed
* Species have a rare Boss variant that cannot spawn on the first map of a WAD
* Species Bosses have a special warning message when they spawn
* Bosses, including Spider Masterminds and Cyberdemons, are not immune to splash damage
* Bosses cannot be gibbed, cannot be revived, are not thrust by damage and are immune to Fear
* Enemies fall into one of five types, with various properties and resistances. Some enemies are multiple types. See below.
* High level elemental attacks can inflict status effects. See below.
* Enemies have powerful Special Attacks that operate on a timer. The timer starts at 0, and goes up as the enemy walks around after being alerted. Only a chance to use when timer is full. Resets to 0 after the Special Attack is used.

# ---  Options ---
* Options for toggling visual effects like elemental VFX like flames and lightning, smoke and dust, and extra gore
* Special Attacks can be toggled. This option also toggles alternate attacks.
* Extra Properties from type and species can be toggled. This does not effect resistances/weakness.
* Elemental Attack Status Effects can be toggled
* Boss Warning Messages can be toggled
* Species Variant spawns can be toggled
* Species Bosses spawns can be toggled

# --- Enemy Types ---
* Humans
  * Does not infight with each other
  * No resistances or weakness
* Demons
  * Come in 3 classes: Lesser, Middle and Greater
  * Resists **Unholy** and **Magic**, weak to **Holy**
  * Middle and Greater demons resist **Elemental** 
  * Middle and Greater demons can be attuned to an Element, further increasing their resistance but granting a weakness
  * Legends tell of an ancient Proto class of demons, resistant to nearly all forms of damage
* Cyber
  * Greatly increased HP
  * Low movement speed with limited turn angles
  * Immune to pain from damage below a certain threshold
  * Weak to **Plasma**, very weak to **Electric**, resists **Physical**
  * **Electric** attacks pain stun with a 100% chance for an extended time
  * Explodes on death
* Undead
  * Increased HP 
  * Immune to Fear
  * Low movement speed
  * Weak to **Fire**, very weak to **Hellfire**, resists **Ice**, immune to **Unholy**
  * **Fire** attacks always cause Burning
  * Corpse must be destroyed or it will revive
  * Cannot be revived normally
* Brain
  * Has a barrier that completely protects them until it is destroyed
    * Barrier resists **Magic** and **LongArms**, weak to **Melee** and **Plasma**
    * Barrier is disrupted and breifly disappears after taking **Melee** damage
  * Weak to **Melee**, greatly resists **Magic**, resists **Elemental**
  * **Melee** type attacks pain stun with a 100% chance
  * Does not infight with each other 

# --- Damage Types and Status ---
* Fire, Ice and Electric are known together as **Elemental**
* Long Arms, Small Arms and Melee are known together as **Physical**
* **Fire** attacks deal typical Doom damage. 
  * Status is **Burning**, dealing damage over time
* **Ice** attacks deal high but very random damage. 
  * Status is **Chilled**, lowering movement speed
* **Electric** attacks deal a set amount of damage. 
  * Status is **Magnetized**, reducing damage dealt and increasing damage taken from **Electric**, **Plasma** and bullets
* **SmallArms** is small caliber bullets, such as 9mm and .45 ACP, that are stopped by soft armor
* **LongArms** is rifle caliber bullets, such as 5.56 and 7.62, that are stopped by hard armor
* **Melee** is melee attacks, and general applications of large amounts of force like psychic pushes or thrown rocks
* **Hellfire** is magical, malicious fire that burns the very soul. *Normal Fire resistance does nothing.*
  * Status is **Hell Burning**, dealing Fire damage over time for a shorter duration, but multiple applications stack duration.
* **Plasma** attacks are quasi-magically attacks generated from weapons built by humans and Brains. Often similar in nature to Electric and possesses electromagnetic properties. Deal high damage and have no Status.
* **Magic** are attacks from the direct application of Mana. Powerful but have no Status. High level, magically inclined enemies tend to resist Magic type damage.
* **Holy** attacks are the power from above.
* **Unholy** attacks are profane in nature, and are often the energy of Death and Undeath. Referred as Grave in relation to the Undead
* **Piercing** attacks can not be resisted by normal means, and often ignore armor
* Damage Types from other mods, only exist as resistances/weakness: **Bullet**, **PLWater**, **Poison**. Undead and Cybers are immune to **Poison**, and enemies that resist **SmallArms**  or **LongArms** also resist **Bullet** to a lesser degree.